import {
  DB,
  Api,
  Route,
  Socket,
  Config,
  AppState,
  BackHandler,
  SplashScreen,
  PushNotification,
} from 'Modules';

import app from 'Logic/App';

import Help from 'Pages/Help';
import Email from 'Pages/Email';
import About from 'Pages/About';
import Video from 'Pages/Video';
import Audio from 'Pages/Audio';
import Login from 'Pages/Login';
import Splash from 'Pages/Splash';
import Signin from 'Pages/Signin';
import SideBar from 'Pages/SideBar';
import Profile from 'Pages/Profile';
import Journal from 'Pages/Journal';
import HomePage from 'Pages/HomePage';
import Register from 'Pages/Register';
import HowToUse from 'Pages/HowToUse';
import Resources from 'Pages/Resources';
import SpeedDial from 'Pages/SpeedDial';
import Statistics from 'Pages/Statistics';
import Appointment from 'Pages/Appointment';
import Notification from 'Pages/Notification';
import Consultation from 'Pages/Consultation';
import ChangePassword from 'Pages/ChangePassword';
import ForgotPassword from 'Pages/ForgotPassword';
import InspiredActions from 'Pages/InspiredActions';
import EventRedirection from 'Pages/EventRedirection';

Route.add('help', Help);
Route.add('email', Email);
Route.add('about', About);
Route.add('media', Video);
Route.add('audio', Audio);
Route.add('login', Login);
Route.add('splash', Splash);
Route.add('signin', Signin);
Route.add('profile', Profile);
Route.add('sideBar', SideBar);
Route.add('journal', Journal);
Route.add('homepage', HomePage);
Route.add('register', Register);
Route.add('how-to-use', HowToUse);
Route.add('resources', Resources);
Route.add('speed-dial', SpeedDial);
Route.add('statistics', Statistics);
Route.add('appointment', Appointment);
Route.add('consultation', Consultation);
Route.add('notification', Notification);
Route.add('change-password', ChangePassword);
Route.add('forgot-password', ForgotPassword);
Route.add('inspired-actions', InspiredActions);
Route.add('event-redirection', EventRedirection);

PushNotification.configure({
  onRegister: function(device){
    app.device = device;
    console.warn(device.token);
  },
  onNotification: function(notification){
    console.warn(notification);
    if(!notification){
      return;
    }
    if(notification.userInfo){
      notification.data = notification.userInfo;
    }
    clearTimeout(app.initPageTimer);
    if(notification.data.local){
      app.onLocalNotification(notification);
    }else{
      app.onPushNotification(notification);
    }
  },
  senderID: Config('GOOGLE_SENDER_ID'),
  permissions:{
    alert: true,
    badge: true,
    sound: true
  },
  popInitialNotification: true,
  requestPermissions: true,
});


BackHandler.addEventListener('hardwareBackPress', () => {
  var route = Route.current();
  if(route._key == 'notification'){
    Route.set('speed-dial');
    return true;
  }else{
    return Route.pop();
  }
});

Route.set('splash');
Route.setDrawer(SideBar);
