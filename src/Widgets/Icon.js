import React, {
  Component
} from 'react';

import {
  Text,
  StyleSheet
} from 'react-native';

const Icons = {
  'account': '\ue900',
  'angle-left':'\ue901',
  'angle-right':'\ue902',
  'angle-up': '\ue903',
  'arrow-left': '\ue904',
  'arrow-right': '\ue905',
  'at-sign':'\ue906',
  'attachment':'\ue907',
  'book':'\ue908',
  'calendar-plus':'\ue909',
  'camera':'\ue90a',
  'check':'\ue90b',
  'check1':'\ue90c',
  'check-circle':'\ue90d',
  'clock': '\ue90e',
  'close':'\ue90f',
  'cog':'\ue910',
  'facebook':'\ue911',
  'fullscreen':'\ue912',
  'google':'\ue913',
  'home':'\ue914',
  'info':'\ue915',
  'ion-edit':'\ue916',
  'ion-ios-calendar-outline':'\ue917',
  'ion-ios-checkmark-outline':'\ue918',
  'ion-ios-compose-outline':'\ue919',
  'ion-ios-help':'\ue91a',
  'ion-locked':'\ue91b',
  'lock': '\ue91c',
  'login':'\ue91d',
  'logout':'\ue91e',
  'mail':'\ue91f',
  'mail-dark':'\ue920',
  'media':'\ue921',
  'menu':'\ue922',
  'mic':'\ue923',
  'more':'\ue924',
  'pause':'\ue925',
  'phone':'\ue926',
  'play':'\ue927',
  'play-outline':'\ue928',
  'plus':'\ue929',
  'profile':'\ue92a',
  'question':'\ue92b',
  'refresh':'\ue92c',
  'resource':'\ue92d',
  'search':'\ue92e',
  'settings':'\ue92f',
  'statistics':'\ue930',
  'user':'\ue931',
  'user-add':'\ue932',
  'user-thin':'\ue933',
  'videocam':'\ue934',
  'youtube':'\ue935',
};

const Icon = (props) => (
  <Text allowFontScaling={false} style={[styles.icon, props.style]}>{Icons[props.name]}</Text>
);

const styles = StyleSheet.create({
  icon: {
    fontSize:18,
    fontFamily: 'IconFamily',
    backgroundColor:  'transparent',
  }
});

export default Icon;
