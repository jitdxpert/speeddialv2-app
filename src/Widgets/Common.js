import React from 'react';
import {
  Text,
  View,
  Image,
  Switch,
  WebView,
  FlatList,
  TextInput,
  StatusBar,
  ScrollView,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';

if(Text.defaultProps == null) {
  Text.defaultProps = {};
}
Text.defaultProps.allowFontScaling = false;

import Video from 'react-native-video';
import CheckBox from 'react-native-check-box';
import ActionSheet from 'react-native-actionsheet';

const Spinner = ActivityIndicator;



export {
  Text,
  View,
  Image,
  Video,
  Switch,
  WebView,
  Spinner,
  FlatList,
  CheckBox,
  TextInput,
  StatusBar,
  ScrollView,
  ActionSheet,
  SafeAreaView,
};
