import React from 'react';
import Fecha from 'fecha';
import { store, view } from 'react-easy-state';
import DatePickerView from 'react-native-date-picker';
import Icon from './Icon';
import {
  Text,
  View,
  Modal,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity
} from 'react-native';


const state = store({
  date: null,
  mode: 'date',
  format: 'YYYY-MM-DD',
  onDateSelect: null,
});

const onSelect = () => {
  state.onDateSelect(Fecha.format(state.date, state.format))
}

const getInitDate = (props) => {
  if(props.format){
    state.format = props.format;
  }
  if(props.mode){
    state.mode = props.mode;
  }
  if(!state.date){
    state.date = new Date();
  }
  state.onDateSelect = props.onSelect;

  return state.date;
};

const DatePicker = view((props) => (

  <Modal
    transparent={true}
    animationType="slide"
    visible={props.show}
    onRequestClose={() => {
      Alert.alert('Modal has been closed.');
    }}>
    <View style={styles.modal}>
      <View style={styles.modalContent}>
        <TouchableOpacity onPress={props.onClose} style={styles.closeButton}>
          <Icon name='close' />
        </TouchableOpacity>
        <TouchableOpacity onPress={onSelect} style={styles.selectButton}>
          <Icon name='check-circle' />
        </TouchableOpacity>
      </View>
      <DatePickerView
        mode={state.mode}
        date={getInitDate(props)}
        onDateChange={(value) => {state.date = value}}/>
    </View>
  </Modal>

));

const styles = StyleSheet.create({
  modal:{
    bottom:0,
    height:260,
    width:'100%',
    paddingBottom:30,
    position:'absolute',
    alignItems:'center',
    backgroundColor:'white',
  },
  modalContent:{
    width:'100%',
    borderTopWidth:1,
    flexDirection:'row',
    alignItems:'center',
    borderTopColor:'#eee',
    justifyContent:'flex-start',
  },
  closeButton:{
    padding:20,
  },
  selectButton:{
    right:0,
    padding:20,
    position:'absolute',
  }
});

export default DatePicker;
