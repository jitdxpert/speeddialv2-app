let _configs = {};
const Config = (key, value) => {
  if(value){
    _configs[key] = value;
  }else{
    value = _configs[key];
  }
  return value;
};

export default Config;
