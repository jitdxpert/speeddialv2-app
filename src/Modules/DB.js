import { AsyncStorage } from 'react-native';

class DB {

  async get(key) {
    let value = await AsyncStorage.getItem(key);
    if (value == null) return null;
    try {
      value = JSON.parse(value);
    } catch (e) {
      value = null;
    }
    return value;
  }

  async set(key, value) {
    value = this.stringify(value);
    try {
      await AsyncStorage.setItem(key, value);
      return true;
    } catch (e) {
      return false;
    }
  }

  async merge(key, value) {
    valuse = this.stringify(value);
    try {
      await AsyncStorage.mergeItem(key, value);
      return true;
    } catch (e) {
      return false;
    }
  }

  stringify(value) {
    if (typeof value === 'object'){
      value = JSON.stringify(value);
    }
    return value;
  }

  async remove(key) {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    } catch (e) {
      return false;
    }
  }

  async clear() {
    try {
      await AsyncStorage.clear();
      return true;
    } catch (e) {
      return false;
    }
  }

}

export default new DB();
