import {
  Alert,
  Linking,
  NetInfo,
  AppState,
  Keyboard,
  ImageEditor,
  BackHandler,
  PermissionsAndroid,
  PushNotificationIOS,
} from 'react-native';

import Toast from 'react-native-simple-toast';
import { store, view } from 'react-easy-state';
import ImageCrop from 'react-native-image-crop-picker';

const JSX = view;
const Model = store;
export {
  JSX,
  Alert,
  Model,
  Toast,
  Linking,
  NetInfo,
  AppState,
  Keyboard,
  ImageCrop,
  ImageEditor,
  BackHandler,
  PermissionsAndroid,
  PushNotificationIOS,
};
