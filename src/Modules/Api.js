import axios from 'axios';
import Config from './Config';


let _TIMER = null;
let _REQUESTS = {};
let _HEADERS = {
  'Accept' : 'application/json',
  'Content-Type' : 'application/json',
  'Access-Control-Allow-Origin' : '*',
};

class Api {

  _url = '/';
  _base = '/';
  _method = 'get';
  _params = {};

  get(url, params = {}){
    this._url = url;
    this._method = 'get';
    this._params = params;
    return this;
  };

  post(url, params = {}) {
    this._url = url;
    this._method = 'post';
    this._params = params;
    return this;
  };

  params(params) {
    this._params = params;
    return this;
  };

  header(id, token) {
    _HEADERS['Auth-Id'] = id;
    _HEADERS['Auth-Token'] = token;
  };

  send(callback){
    _REQUESTS[this._url] = {
      _url: this._url,
      _method: this._method,
      _params: this._params,
      callback: callback,
    };

    let _self = this;
    clearTimeout(_TIMER);
    _TIMER = setTimeout(()=>{
      _self.processApiRequest();
    }, 1000);
  };

  processApiRequest(){

    let _keys = Object.keys(_REQUESTS);
    if(!_keys.length){
      return;
    }

    let _self = _REQUESTS[_keys[0]];
    delete(_REQUESTS[_keys[0]]);
    _self._headers = _HEADERS;
    _self._base_url = Config('API_BASE');
    _self.processApiRequest = this.processApiRequest;

    let request = {
      method: _self._method,
      headers: _self._headers,
      url: `${_self._base_url}${_self._url}`
    };

    if (_self._method === 'post') {
      request.data = _self._params;
    } else if (_self._method === 'get') {
      request.params = _self._params;
    }


    axios(request)
    .then((response) => {
      _self.processApiRequest();
      _self.callback(response.data);
    })
    .catch((error) => {
      _self.processApiRequest();
      _self.callback({type:'error', text : 'API Server Error'});
    });

  };


}

export default new Api();
