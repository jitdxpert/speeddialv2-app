import DB from './DB';
import Api from './Api';
import Fecha from 'fecha';
import Route from './Route';
import Config from './Config';
import { Style, Screen } from './Design';
import SplashScreen from 'react-native-splash-screen';
// import PushNotification from 'react-native-push-notification';
var PushNotification = require('react-native-push-notification');

import {
  JSX,
  AWS3,
  Alert,
  Model,
  Toast,
  Linking,
  NetInfo,
  Keyboard,
  AppState,
  ImageCrop,
  ImageEditor,
  BackHandler,
  PermissionsAndroid,
  PushNotificationIOS,
} from './Common';


import { Platform } from 'react-native';


export {
  DB,
  Api,
  JSX,
  AWS3,
  Alert,
  Fecha,
  Route,
  Model,
  Toast,
  Style,
  Config,
  Screen,
  Linking,
  NetInfo,
  Keyboard,
  AppState,
  Platform,
  ImageCrop,
  ImageEditor,
  BackHandler,
  SplashScreen,
  PushNotification,
  PermissionsAndroid,
  PushNotificationIOS,
}
