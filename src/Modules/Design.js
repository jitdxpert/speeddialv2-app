import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

const { width, height } = Dimensions.get('window');

const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const Style = StyleSheet.create;
const Screen = Dimensions.get('window');

Style.vw = (size, factor=0) => {
    let scaleSize = width / guidelineBaseWidth * size;
    if(factor){
      scaleSize = size + (scaleSize - size) * factor;
    }
    return scaleSize;
};

Style.vh = (size, factor=0) => {
  let scaleSize = height / guidelineBaseWidth * size;
  if(factor){
    scaleSize = size + (scaleSize - size) * factor;
  }
  return scaleSize;
};

Style.width = width;
Style.height = height;

export {
  Style,
  Screen,
};
