import React from 'react';
import {
  Text,
  View,
  Easing,
  Animated,
  Dimensions,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';

import { store, view } from 'react-easy-state';
let screen = Dimensions.get('window');


const Route = {};

Route._cache = {};
Route._history = [];
Route._prevPage = null;

Route._model = store({
  current: 'none',
  drawer: 'none'
});

const opacity  = new Animated.Value(1);
const translateY = new Animated.Value(0);
const translateX = new Animated.Value(-screen.width);


Route.add = (key, page) => {
  page._key = key;
  Route._cache[key] = page;
}

Route.get = (key) => {
  if(!Route._cache[key]){
    return {template:View404};
  }
  return Route._cache[key];
}

Route.current = () => {
  return Route.get(Route._model.current);
}

Route.data = (key, prop) => {
  let data = {};
  if(!Route._cache[key]){
    return data;
  }
  data = Route._cache[key];
  if(!data[prop]){
    return null;
  }
  return data[prop];
}


Route.init = () => {
  Route.loadAnimate();
  return Route._jsx;
};


Route.load = (key, params={}) => {
  Route._prevTemplate = Route.get(Route._model.current).template;
  Route._model.current = key;
  let page = Route.get(key);
  if(page.init){
    page.init(params);
  }
  Route.loadAnimate();
};


Route.resume = (key, params={}) => {
  Route._model.current = key;
  let page = Route.get(key);
  if(page.resume){
    page.resume(params);
  }
};

Route.set = (key, params={}) => {
  Route._history = [{key, params}];
  Route.load(key, params);
};

Route.push = (key, params={}) => {
  Route._history.push({key, params});
  Route.load(key, params);
  Route.animate();
};

Route.pop = (params) => {
  if(Route._history.length < 2){
    return false;
  }
  Route._history.pop();
  let history = Route._history[Route._history.length-1];
  if(!params){
    params = history.params;
  }
  setTimeout(()=>{
    translateY.setValue(1);
    Route.resume(history.key, params);
  },300);
  Route.popAnimate();
  return true;
};

Route.fire = (key, event, params={}) => {
  let page = Route.get(key);
  if(page[event]){
    page[event](params);
  }else{
    console.warn('No page event found');
  }
}

Route.setDrawer = (template) => {
  Route.add('drawer', template);
  Route._drawerState = 'close';
  Route._model.drawer = 'drawer';
};

Route.drawerIsOpen = ()=>{
  return Route._drawerState == 'open';
};

Route.toggleDrawer = (template)=>{
  if(Route._drawerState = 'open'){
    Route.closeDrawer();
  }else{
    Route.openDrawer();
  }
};

Route.openDrawer = ()=>{
  Route.fire('drawer', 'open');
  Route._drawerState = 'open';
  Route.drawerAnimate();
};

Route.closeDrawer = ()=>{
  Route.fire('drawer', 'close');
  Route._drawerState = 'close';
  Route.drawerAnimate();
};




const styles = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'
  }
});


const View404 = () => (
  <View style={styles.container}>
    <Text>Page :</Text>
    <Text style={{color:'#900'}}>{Route._model.current}</Text>
    <Text> Not Found</Text>
  </View>
);




class Router extends React.Component{

  render(){

    const Page = Route.get(Route._model.current);
    const Drawer = Route.get(Route._model.drawer);
    const PageTemplate = Page.template;
    const DrawerTemplate = Drawer.template;

    return (
      <View style={{flex:1}}>
        <View style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size="large" color="#777"/>
        </View>
        <Animated.View style={{
            flex:1,
            opacity: opacity,
            backgroundColor: '#fff',
            transform: [{translateY: translateY}],
          }} >
          <PageTemplate />
        </Animated.View>
        <Animated.View style={{
            width: '100%',
            height: '100%',
            position: 'absolute',
            transform: [{translateX: translateX}],
          }} >
          <Animated.View style={{
                width: '100%',
                height: '100%',
                position: 'absolute',
                backgroundColor: '#55555555',
                opacity: drawerOpacity,
          }} />
          <DrawerTemplate />
        </Animated.View>
      </View>
    )
  }
}




Route.loadAnimate = () => {
  opacity.setValue(0);
  Animated.timing(
    opacity,
    {
      toValue: 1,
      delay: 100,
      duration: 200
    }
  ).start();
}

Route.popAnimate = () => {
  Animated.timing(
    translateY,
    {
      toValue: 800,
      duration: 400,
      easing: Easing.bezier(0,0.6,0.4,1)
    }
  ).start();

  Animated.sequence([
    Animated.timing(opacity, {
      toValue: 0,
      duration: 200
    }),
    Animated.timing(opacity, {
      toValue: 1,
      duration: 200
    })
  ]).start();
}

Route.animate = () => {
  translateY.setValue(100);
  Animated.timing(
    translateY,
    {
      toValue: 0,
      duration: 400,
      easing: Easing.bezier(0,0.6,0.4,1)
    }
  ).start();
}

const drawerOpacity = new Animated.Value(0);
Route.drawerAnimate = () => {
  let toValue = 0;
  if(!Route.drawerIsOpen()){
    toValue = -screen.width;
  }
  drawerOpacity.setValue(0);
  Animated.timing(
    drawerOpacity,
    {
      toValue: 1,
      delay: 300,
      duration: 100
    }
  ).start();
  Animated.timing(
    translateX,
    {
      toValue: toValue,
      duration: 400,
      easing: Easing.bezier(0,0.6,0.4,1)
    }
  ).start();
};

Dimensions.addEventListener('change', () => {
  screen = Dimensions.get('window');
  Route.drawerAnimate();
});

Route._jsx = view(Router);

export default Route;
