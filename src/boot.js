require('./config');
require('./routes');

import {
  Route,
  Socket,
} from 'Modules';

export default Route.init();
