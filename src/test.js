import React, {Component} from 'react';
import Video from 'react-native-video';
import RNPrint from 'react-native-print';
import Toast from 'react-native-simple-toast';
import DatePicker from 'react-native-date-picker';
import ActionSheet from 'react-native-actionsheet';
import SplashScreen from 'react-native-splash-screen';
import ImagePicker from 'react-native-image-crop-picker';
var PushNotification = require('react-native-push-notification');
import {GoogleSignin, statusCodes} from 'react-native-google-signin';
import {LoginManager, AccessToken, GraphRequest, GraphRequestManager} from 'react-native-fbsdk';
import {
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  TextInput,
  Platform,
  Text,
  View,
} from 'react-native';


const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 4;
const asOptions = ['Cancel', 'Apple', 'Banana', 'Watermelon', 'Durian'];
const title = 'Which one do you like ?';
const message = 'In botany, a fruit is the seed-bearing structure in flowering plants (also known as angiosperms) formed from the ovary after flowering.';

export default class App extends Component{

  constructor(props){
    super(props);

    this.state = {
      token: '',
      selected: '',
      date: new Date(),
    };
  }

  componentWillMount(){
    GoogleSignin.hasPlayServices({ autoResolve: true }).then(() => {
      //
    }).catch((err)=>{
      console.warn("Play services error", err.code, err.message);
    })

    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      webClientId: '149145940760-3fboc1jo8579p3dejirvpvuk6q866830.apps.googleusercontent.com',
      offlineAccess: true,
      hostedDomain: '',
      forceConsentPrompt: true,
      accountName: '',
      iosClientId: '149145940760-2jhme93hq97a8vq2hr7nn14nhc6i6fbm.apps.googleusercontent.com',
    });
  }

  componentDidMount(){
    let self = this;
    PushNotification.configure({
      onRegister: function(device){
        console.warn(device);
        self.setState({ token: device.token })
      },
      onNotification: function(notification){
        console.warn(notification);
      },
      senderID: '149145940760',
      largeIcon: "icon",
      smallIcon: "icon",
    });

    SplashScreen.hide();
  }

  async printHtml(){
    await RNPrint.print({
      html: '<h1>Heading 1</h1><h2>Heading 2</h2><h3>Heading 3</h3>'
    });
  }

  openGallery(){
    let self = this;
    ImagePicker.openPicker({
      width: 200,
      height: 200,
      cropping: true
    }).then(image => {
      Toast.show(image.path);
      self.uploadImage(image.path);
    });
  }

  uploadImage(imagePath){

  }

  showToast(){
    Toast.show('This is a toast.');
  }

  player(){

  }

  onBuffer(){

  }

  videoError(){

  }

  showActionSheet = () => {
    this.ActionSheet.show();
  }

  handlePress = (buttonIndex) => {
    this.setState({ selected: buttonIndex })
  }

  async googleLogin(){
    Toast.show('Google');
    try{
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      Toast.show(userInfo.user.name);
    }catch(error){
      if(error.code === statusCodes.SIGN_IN_CANCELLED){
        Toast.show('Login was cancelled');
      }else if(error.code === statusCodes.IN_PROGRESS){
        Toast.show('Login in progress');
      }else if(error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE){
        Toast.show('Play services not available');
      }else{
        Toast.show('Error happen while login');
      }
    }
  }

  fbInfoCallback = (error, result) => {
    if(error){
      Toast.show('Error');
    }else{
      Toast.show(result.name);
    }
  }

  facebookLogin(){
    Toast.show('Facebook');
    let self = this;
    LoginManager.logInWithReadPermissions(['public_profile']).then(function(result){
      if(result.isCancelled){
        Toast.show('Login was cancelled');
      }else{
        AccessToken.getCurrentAccessToken().then((data) => {
          const infoRequest = new GraphRequest('/me?fields=id,name,email,picture', null,  self.fbInfoCallback);
          new GraphRequestManager().addRequest(infoRequest).start();
        });
      }
    }, function(error){
      Toast.show('Login failed with error:'+error);
    });
  }

  render(){
    return(
      <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps="handled">

        <TextInput style={styles.input} value={this.state.token} multiline={true} />

        <View style={styles.videoContainer}>
          <Video source={{uri: "https://masterpeacecoaching.com/speeddialapp/uploads/videos/1542813057-videoplayback.mp4"}}
            ref={(ref) => { this.player = ref }}
            controls={true}
            onBuffer={this.onBuffer}
            onError={this.videoError}
            style={styles.backgroundVideo}
          />
        </View>

        <Text style={styles.actionMessage}>I like {asOptions[this.state.selected] || '...'}</Text>

        <TouchableHighlight style={styles.fullWidthButton} onPress={this.googleLogin}>
          <Text style={styles.fullWidthButtonText}>Login With Google</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.fullWidthButton} onPress={this.facebookLogin}>
          <Text style={styles.fullWidthButtonText}>Login With Facebook</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.fullWidthButton} onPress={this.printHtml}>
          <Text style={styles.fullWidthButtonText}>Print</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.fullWidthButton} onPress={this.showToast}>
          <Text style={styles.fullWidthButtonText}>Show Toast</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.fullWidthButton} onPress={this.openGallery}>
          <Text style={styles.fullWidthButtonText}>Open Gallery</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.fullWidthButton} onPress={this.showActionSheet}>
          <Text style={styles.fullWidthButtonText}>Open ActionSheet</Text>
        </TouchableHighlight>


        <DatePicker
          date={this.state.date}
          onDateChange={date => this.setState({ date })}
        />

        <ActionSheet
          ref={o => { this.ActionSheet = o }}
          title={title}
          message={message}
          options={asOptions}
          cancelButtonIndex={CANCEL_INDEX}
          destructiveButtonIndex={DESTRUCTIVE_INDEX}
          onPress={this.handlePress}
        />

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:'#f5f5f5',
  },
  input:{
    paddingVertical:10,
    paddingHorizontal:20,
  },
  videoContainer:{
    height:300,
  },
  backgroundVideo:{
    top:10,
    left:20,
    right:20,
    bottom:10,
    position:'absolute',
    backgroundColor:'black',
  },
  fullWidthButton:{
    height:60,
    marginVertical:10,
    flexDirection:'row',
    marginHorizontal:20,
    alignItems:'center',
    backgroundColor:'blue',
    justifyContent:'center',
  },
  fullWidthButtonText:{
    fontSize:20,
    color:'white',
  },
  actionMessage:{
    marginVertical:10,
    marginHorizontal:20,
  }
});
