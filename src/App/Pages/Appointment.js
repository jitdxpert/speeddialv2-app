import React from 'react';
import { JSX, Style, Platform } from 'Modules';
import {
  WebView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Appointment';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Common';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={true}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Book Appointment"
    />

    <WebView
      style={styles.body}
      originWhitelist={['*']}
      source={page.calenderUri}
      scalesPageToFit={Platform.OS !== 'ios'}
      onLoadStart={() => {app.loading = true}}
      onLoad={() => {app.loading = false}}
    />

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
  },
  body:{
    paddingVertical:15,
    paddingHorizontal:20,
    backgroundColor:'#f5f5f5',
  },
});

export default page;
