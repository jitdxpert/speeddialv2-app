import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Button,
  DatePicker,
  ScrollView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Statistics';
import Chart from 'Shared/Chart';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Common';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={false}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Statistics"
    />

    <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

      <View style={styles.firstRow}>

        <View style={styles.listItemTop}>
          <View style={styles.changeDateBtn}>
            <Text style={styles.listTextTop}>Statistics for:</Text>
            <Button onPress={page.onPressChangeDate}>
              <Text style={styles.listStatDate}>{page.choosenDate}</Text>
            </Button>
          </View>
        </View>

        {page.total ? (
          <Chart pending={page.pending} complete={page.complete} />
        ) : (
          <View style={[styles.listItem, styles.noTopRadius]}>
            <Text style={styles.listText}>No stats to show in the graph</Text>
          </View>
        )}

      </View>


      <View style={styles.listItem}>
        <Text
          style={styles.listText}
          numberOfLines={1}
          ellipsizeMode={'tail'}>Inspired Actions set</Text>
        <View style={styles.buttonTextIcon}>
          <Text style={styles.buttonText}>{page.total}</Text>
        </View>
      </View>

      <View style={styles.listItem}>
        <Text
          style={styles.listText}
          numberOfLines={1}>Inspired Actions Completed</Text>
        <View style={styles.buttonTextIcon}>
          <Text style={styles.buttonText}>{page.complete}</Text>
        </View>
      </View>

      <View style={styles.listItem}>
        <Text
          style={styles.listText}
          numberOfLines={1}
          ellipsizeMode={'tail'}>Percentage Completed</Text>
        <View style={styles.buttonTextIcon}>
          <Text style={styles.buttonText}>{page.percent}%</Text>
        </View>
      </View>

    </ScrollView>

    <DatePicker
      show={page.showDatePicker}
      mode={'date'}
      format={'YYYY-MM-DD'}
      onSelect={page.onSelectDate}
      onClose={() => {page.showDatePicker = false}}
    />

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#fff',
  },
  body:{
    padding:20,
  },
  firstRow:{
    marginBottom:25,
  },
  listItem:{
    borderRadius:5,
    borderWidth:0.5,
    marginBottom:10,
    borderColor:'#ddd',
    alignItems:'center',
    flexDirection:'row',
    paddingVertical:14,
    paddingHorizontal:20,
    backgroundColor:'white',
    justifyContent:'space-between',
  },
  noTopRadius:{
    borderTopLeftRadius:0,
    borderTopRightRadius:0,
  },
  chartItem:{
    height:350,
  },
  listItemTop:{
    height:50,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    backgroundColor:'#e05729',
    justifyContent:'space-between',
  },
  buttonTextIcon:{
    width:50,
    height:25,
    borderRadius:8,
    alignItems:'center',
    backgroundColor:'green',
    justifyContent:'center'
  },
  buttonText:{
    color:'white',
  },

  listText:{
    color:'#696969',
    width:'80%'
  },

  changeDateBtn:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
  },
  listTextTop:{
    color:'white',
    fontWeight:'bold',
  },
  listStatDate:{
    color:'white',
    fontWeight:'bold',
    textAlign:'right',
    textDecorationLine:'underline',
  },
  lastLineView:{
    marginTop:40,
    alignItems:'center',
    justifyContent:'center',
  },
  lastLine:{
    fontSize:12,
    color:'grey',
  },
});

export default page;
