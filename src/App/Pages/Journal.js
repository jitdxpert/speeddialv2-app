import React from 'react';
import { JSX, Style, Platform } from 'Modules';
import {
  Text,
  View,
  Icon,
  Button,
  TextInput,
  ScrollView,
  ActionSheet,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Journal';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Common';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={true}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      onPressIA={page.onPressInspireAction}
      title={page.date.format}
    />

    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : ''}>

      <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

        <View style={styles.heading}>
          <Text style={styles.headingText}>TYPE IN YOUR ANSWERS BELOW:</Text>
        </View>

        <View style={styles.question}>
          <Text style={styles.questionText}>Today, I appreciate:</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.question1Input = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Today, I appreciate"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={4}
          multiline={true}
          onSubmitEditing={() => {page.question2Input.focus()}}
          onChangeText={(value) => {page.question1 = value}}
          value={page.question1}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>Today my dominant feeling will be:</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.question2Input = input}}
          placeholder={"Today my dominant feeling will be"}
          underlineColorAndroid={"transparent"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={4}
          multiline={true}
          onSubmitEditing={() => {page.rock1Input.focus()}}
          onChangeText={(value) => {page.question2 = value}}
          value={page.question2}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>My 6 inspired actions for today are:</Text>
        </View>
        <TextInput style={styles.textInputDesignForActions}
          ref={(input) => {page.rock1Input = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Inspired Action 1"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={3}
          multiline={true}
          onSubmitEditing={() => {page.rock2Input.focus()}}
          onChangeText={(value) => {page.rocks.rock1 = value}}
          value={page.rocks.rock1}
        />
        <TextInput style={[styles.textInputDesignForActions, styles.topRadiusEnabled]}
          ref={(input) => {page.rock2Input = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Inspired Action 2"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={3}
          multiline={true}
          onSubmitEditing={() => {page.rock3Input.focus()}}
          onChangeText={(value) => {page.rocks.rock2 = value}}
          value={page.rocks.rock2}
        />
        <TextInput style={[styles.textInputDesignForActions, styles.topRadiusEnabled]}
          ref={(input) => {page.rock3Input = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Inspired Action 3"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={3}
          multiline={true}
          onSubmitEditing={() => {page.rock4Input.focus()}}
          onChangeText={(value) => {page.rocks.rock3 = value}}
          value={page.rocks.rock3}
        />
        <TextInput style={[styles.textInputDesignForActions, styles.topRadiusEnabled]}
          ref={(input) => {page.rock4Input = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Inspired Action 4"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={3}
          multiline={true}
          onSubmitEditing={() => {page.rock5Input.focus()}}
          onChangeText={(value) => {page.rocks.rock4 = value}}
          value={page.rocks.rock4}
        />
        <TextInput style={[styles.textInputDesignForActions, styles.topRadiusEnabled]}
          ref={(input) => {page.rock5Input = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Inspired Action 5"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={3}
          multiline={true}
          onSubmitEditing={() => {page.rock6Input.focus()}}
          onChangeText={(value) => {page.rocks.rock5 = value}}
          value={page.rocks.rock5}
        />
        <TextInput style={[styles.textInputDesignForActions, styles.topRadiusEnabled]}
          ref={(input) => {page.rock6Input = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Inspired Action 6"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={3}
          multiline={true}
          onSubmitEditing={() => {page.question3Input.focus()}}
          onChangeText={(value) => {page.rocks.rock6 = value}}
          value={page.rocks.rock6}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>Everything else I desire is being delegated to the Universe. Thank You Universe, for handling the following for me!</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.question3Input = input}}
          placeholder={"Everything else I desire is being delegated to the Universe.Thank you Universe, for handling the following for me!"}
          underlineColorAndroid={"transparent"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={4}
          multiline={true}
          onSubmitEditing={() => {page.question4Input.focus()}}
          onChangeText={(value) => {page.question3 = value}}
          value={page.question3}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>My Wild Hair Intention. Wouldn't it be awesome today if:</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.question4Input = input}}
          placeholder={"My Wild Hair Intention. Wouldnt it be awesome today if:"}
          underlineColorAndroid={"transparent"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={4}
          multiline={true}
          onSubmitEditing={() => {page.question5Input.focus()}}
          onChangeText={(value) => {page.question4 = value}}
          value={page.question4}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>One of my desires has manifested in the most perfect way. It unfolded like this..</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.question5Input = input}}
          placeholder={"One of my desires has manifested in the most perfect way. It unfolded like this.."}
          underlineColorAndroid={"transparent"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={4}
          multiline={true}
          onSubmitEditing={() => {page.question6Input.focus()}}
          onChangeText={(value) => {page.question5 = value}}
          value={page.question5}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>The most important thing for me to focus today is..</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.question6Input = input}}
          placeholder={"The most important thing for me to focus today is.."}
          underlineColorAndroid={"transparent"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"done"}
          autoCorrect={true}
          numberOfLines={4}
          multiline={true}
          onSubmitEditing={page.onPressSaveJournal}
          onChangeText={(value) => {page.question6 = value}}
          value={page.question6}
        />

        <View style={styles.buttonView}>
          <Button style={styles.buttonDesignSaveJournal} onPress={page.onPressSaveJournal}>
            <Text style={styles.buttonText}>Save</Text>
            <Icon name="calendar-plus" style={styles.iconCalender}/>
          </Button>

          <Button style={styles.buttonDesignPrint} onPress={page.onPressEmailPrint}>
            <Text style={styles.buttonText}>Send</Text>
            <Icon name="mail" style={styles.iconMail}/>
          </Button>
        </View>

        <View style={{height:100}}></View>

      </ScrollView>

    </KeyboardAvoidingView>

    <Loader show={app.loading} />

    <ActionSheet
      ref={page.onActionSheetInit}
      title={'Choose Option'}
      options={['Print', 'Email', 'Cancel']}
      cancelButtonIndex={2}
      destructiveButtonIndex={2}
      onPress={page.onActionSheetPress}
    />

  </SafeAreaView>
));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  body:{
    padding:20,
  },
  textInputDesign:{
    marginTop:-2,
    borderWidth:1,
    minHeight:100,
    marginBottom:10,
    borderColor:'#ddd',
    paddingVertical:10,
    paddingHorizontal:10,
    backgroundColor:'white',
    borderBottomLeftRadius:5,
    borderBottomRightRadius:5,
  },
  textInputDesignForActions:{
    minHeight:80,
    marginTop:-2,
    borderWidth:1,
    marginBottom:10,
    borderColor:'#ddd',
    paddingVertical:10,
    paddingHorizontal:10,
    backgroundColor:'white',
    borderBottomLeftRadius:5,
    borderBottomRightRadius:5,
  },
  topRadiusEnabled:{
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
  },
  buttonView:{
    borderColor:'#fff',
    flexDirection:'row',
    paddingVertical:10,
    alignItems:'center',
    justifyContent:'space-between'
  },
  buttonDesignSaveJournal:{
    height:40,
    width:'48%',
    marginTop:15,
    borderRadius:5,
    borderColor:'#ddd',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#78e08f',
  },
  buttonDesignPrint:{
    height:40,
    width:'48%',
    marginTop:15,
    borderRadius:5,
    borderColor:'#ddd',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#89cff0',
  },
  iconMail:{
    right:10,
    fontSize:18,
    color:'white',
    position:'absolute',
  },
  iconCalender:{
    right:10,
    fontSize:16,
    color:'white',
    position:'absolute',
  },
  buttonText:{
    fontSize:16,
    color:'white',
  },
  question:{
    borderWidth:1,
    paddingVertical:10,
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:10,
    borderColor:'#fa6304',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    backgroundColor:'#fa6304',
    justifyContent:'space-between',
  },
  questionText:{
    fontSize:15,
    color:'white',
    fontWeight:'bold',
  },
  heading:{
    height:50,
    borderRadius:5,
    marginBottom:10,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:10,
    backgroundColor:'grey',
    justifyContent:'space-between',
  },
  headingText:{
    fontSize:15,
    color:'white',
    fontWeight:'bold',
  },
});

export default page;
