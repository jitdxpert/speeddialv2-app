import React from 'react';
import { JSX, Style, Platform } from 'Modules';
import {
  View,
  WebView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/About';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Common';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={false}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="About"
    />

    <WebView
      style={styles.content}
      originWhitelist={['*']}
      scalesPageToFit={Platform.OS !== 'ios'}
      source={{html: '<div style="padding:2px 15px">'+page.content+'</div>'}}
    />

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
  },
  content:{
    backgroundColor:'#f5f5f5',
  },
});

export default page;
