import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Video,
  Button,
  Spinner,
  ScrollView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Audio';
import Empty from 'Shared/Empty';
import Loader from 'Shared/Loader';
import Footer from 'Shared/Footer';
import Header from 'Shared/Headers/Common';

const isAudioLoading = (loading) => {
  return loading ? <Spinner style={styles.spinner} size="small" color="#fff" /> : null;
};

const AudioLoader = JSX((props) => (
  isAudioLoading(props.show)
));

const audioCard = (audios) => {
  if(!audios.length){
    return <Empty icon={page.icon} message={page.message} />
  }

  return audios.map((audio, i) => (
    <View key={i} style={styles.audioPlayer}>
      <View style={styles.audioTitle}>
        <Text style={styles.audioTitleText}>{audio.Audio_Title}</Text>
      </View>
      <Video
        audioOnly={true}
        source={{uri: encodeURI(page.baseurl+'/uploads/audios/'+audio.Audio_File)}}
        paused={audio.paused}
        onEnd={() => {page.onEnd(audio)}}
        onLoadStart={() => {page.onLoadStart(audio)}}
        onLoad={(data) => {page.onLoad(data, audio)}}
        onProgress={(data) => {page.onProgress(data, audio)}}
        repeat={true}
      />
      <View style={styles.audioBar}>
        <View style={styles.audioBarLeft}>
          <Button onPress={() => {page.onPlay(audio, i)}} disabled={audio.loading}>
            <Icon style={styles.icon} name={audio.playPauseIcon} />
          </Button>
          <AudioLoader show={audio.loading} />
        </View>
        <View style={styles.audioBarRight}>
          <Text style={styles.audioBarText}>{audio.currentTime}</Text>
          <Text style={styles.audioBarText1}>{audio.duration}</Text>
        </View>
      </View>
    </View>
  ));
};

page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={false}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Audio"
    />

    <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

      {audioCard(page.audios)}

    </ScrollView>

    <Footer active={1} onPress={page.onMediaTabChange} />

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  body:{
    paddingHorizontal:20,
    backgroundColor:'#f5f5f5',
  },
  audioTitle:{
    elevation:1,
    marginTop:20,
    paddingVertical:15,
    paddingHorizontal:20,
    backgroundColor:'white',
    justifyContent:'center',
  },
  audioBar:{
    height:50,
    elevation:1,
    alignItems:'center',
    flexDirection:'row',
    backgroundColor:'black',
  },
  audioBarLeft:{
    width:'30%',
    flexDirection:'row',
    justifyContent:'space-between',
  },
  audioBarRight:{
    flex:1,
    flexDirection:'row',
    paddingVertical:10,
    paddingHorizontal:20,
    justifyContent:'space-between',
  },
  spinner:{
    marginRight:15,
  },
  audioTitleText:{
    color:'black',
    fontWeight:'bold',
  },
  audioBarText:{
    fontSize:16,
    color:'white',
  },
  audioBarText1:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
  },
  icon:{
    fontSize:18,
    color:'white',
    paddingVertical:10,
    paddingHorizontal:20,
  },
});

export default page;
