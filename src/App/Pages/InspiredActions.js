import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Button,
  CheckBox,
  ScrollView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/InspiredActions';
import Header from 'Shared/Headers/Common';
import Loader from 'Shared/Loader';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={page.hasBack}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Set Inspired Actions"
    />

    <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

      <View style={styles.heading}>
        <Text style={styles.headingText}>Inspired Actions for {page.date}:</Text>
      </View>

      <View style={styles.actionBox}>
        {page.items.map((item, i) => (
          <View key={i}>
            {item.Answer ? (
              <View style={styles.row}>
                <CheckBox isChecked={item.Rock_Status=='complete'} onClick={() => {page.onToggleComplete(item)}} />
                <Text style={styles.rowText}>{item.Answer}</Text>
              </View>
            ) : null}
          </View>
        ))}
      </View>

      <Button style={styles.buttonDesign} onPress={page.saveInspiredActions}>
        <Text style={styles.buttonText}>Submit</Text>
        <Icon name="check-circle" style={styles.icon}/>
      </Button>

    </ScrollView>

    <Loader show={app.loading} />

  </SafeAreaView>
));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  body:{
    padding:20,
  },
  row:{
    borderRadius:5,
    marginBottom:10,
    borderWidth:0.5,
    paddingVertical:10,
    borderColor:'#ddd',
    alignItems:'center',
    flexDirection:'row',
    position:'relative',
    paddingHorizontal:10,
    backgroundColor:'white',
  },
  rowText:{
    fontSize:15,
    marginLeft:10,
    marginRight:30,
  },
  actionBox:{
    minHeight:300,
  },
  buttonDesign:{
    height:40,
    marginTop:20,
    borderRadius:5,
    borderColor:'#ddd',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    justifyContent:'center',
    backgroundColor:'#78e08f',
  },
  icon:{
    right:10,
    fontSize:18,
    color:'white',
    position:'absolute',
  },
  buttonText:{
    fontSize:16,
    color:'white',
  },
  heading:{
    height:50,
    borderRadius:5,
    marginBottom:10,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    backgroundColor:'#fa6304',
    justifyContent:'space-between',
  },
  headingText:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
  },
});

export default page;
