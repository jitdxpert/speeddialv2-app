import React from 'react';
import { JSX, Style, Platform } from 'Modules';
import {
  Text,
  View,
  Icon,
  Image,
  Button,
  StatusBar,
  TextInput,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Register';
import Loader from 'Shared/Loader';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <StatusBar
      backgroundColor="white"
      barStyle="dark-content"
    />

    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : ''}>

      <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

        <Image style={styles.imgStyle} source={page.bannerUri}/>

        <View style={styles.form}>

          <View style={styles.listItem}>
            <Text style={styles.labelInput}>Full Name</Text>
            <View style={styles.listItem1}>
              <TextInput style={styles.input}
                ref={(input) => {page.nameInput = input}}
                underlineColorAndroid={"transparent"}
                autoCapitalize={"words"}
                textContentType={"name"}
                returnKeyType={"next"}
                blurOnSubmit={false}
                autoCorrect={false}
                onSubmitEditing={() => {page.emailInput.focus()}}
                onChangeText={(value) => {page.name = value}}
                value={page.name}
              />
              <Icon name="user-thin" style={styles.textInputIcon} />
            </View>
          </View>

          <View style={styles.listItem}>
            <Text style={styles.labelInput}>Email Address</Text>
            <View style={styles.listItem1}>
              <TextInput style={styles.input}
                ref={(input) => {page.emailInput = input}}
                underlineColorAndroid={"transparent"}
                keyboardType={"email-address"}
                autoCapitalize={"none"}
                returnKeyType={"next"}
                blurOnSubmit={false}
                autoCorrect={false}
                onSubmitEditing={() => {page.passwordInput.focus()}}
                onChangeText={(value) => {page.email = value}}
                value={page.email}
              />
              <Icon name="mail-dark" style={styles.textInputIcon} />
            </View>
          </View>

          <View style={styles.listItem}>
            <Text style={styles.labelInput}>Password</Text>
            <View style={styles.listItem1}>
              <TextInput style={styles.input}
                ref={(input) => {page.passwordInput = input}}
                underlineColorAndroid={"transparent"}
                autoCapitalize={"none"}
                secureTextEntry={true}
                returnKeyType={"done"}
                autoCorrect={false}
                onSubmitEditing={page.onRegisterProcess}
                onChangeText={(value) => {page.password = value}}
                value={page.password}
              />
              <Icon name="ion-locked" style={styles.textInputIcon} />
            </View>
          </View>
        </View>

        <Button style={styles.buttonBody} onPress={page.onRegisterProcess}>
          <Text style={styles.buttonTextRegister}>Register</Text>
          <Icon name="user-add" style={styles.iconButton} />
        </Button>

        <View style ={styles.textButtonView}>
          <Button style={styles.backToHomeButton} onPress={page.onPressBackToHome}>
            <Icon name="arrow-left" style={styles.iconArrow} />
            <Text style={styles.textBackHome}>Back to Home</Text>
          </Button>

          <Button style={styles.backToHomeButton} onPress={page.onPressLogin}>
            <Text style={styles.textForgotPass}>Login</Text>
            <Icon name="arrow-right" style={styles.iconArrow} />
          </Button>
        </View>

        <View style={{height:100}}></View>

      </ScrollView>

    </KeyboardAvoidingView>

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
  },
  body:{
    alignItems:'center',
    paddingHorizontal:20,
  },
  imgStyle:{
    width:350,
    height:250,
  },
  form:{
    elevation:1,
    width:'100%',
    borderRadius:5,
    borderWidth:0.05,
    overflow:'hidden',
    borderBottomWidth:0,
  },
  listItem:{
    padding:15,
    width:'100%',
    borderColor:'#ddd',
    borderBottomWidth:1,
    flexDirection:'column',
    justifyContent:'center',
  },
  labelInput:{
    marginBottom:10,
  },
  listItem1:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
  },
  input:{
    height:35,
    fontSize:14,
    width:'95%',
  },
  textInputIcon:{
    fontSize:20,
    color:'black',
  },
  buttonBody:{
    height:40,
    width:'100%',
    marginTop:20,
    borderRadius:5,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#636e72',
  },
  buttonTextRegister:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
  },
  iconButton:{
    right:10,
    fontSize:20,
    color:'white',
    position:'absolute',
  },
  textButtonView:{
    width:'100%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
  },
  backToHomeButton:{
    marginTop:20,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
  },
  iconArrow:{
    color:'grey',
  },
  textBackHome:{
    fontSize:12,
    color:'grey',
    marginLeft:5,
  },
  textForgotPass:{
    fontSize:12,
    color:'grey',
    marginRight:5,
    marginLeft:60,
  },
});

export default page;
