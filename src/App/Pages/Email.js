import React from 'react';
import { JSX, Style, Platform } from 'Modules';
import {
  View,
  Text,
  Icon,
  Button,
  TextInput,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Email';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Common';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={true}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Send Inspired Actions"
    />

    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : ''}>

      <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

        <View style={styles.question}>
          <Text style={styles.questionText}>Email:</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.emailInput = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Enter email"}
          keyboardType={"email-address"}
          placeholderTextColor={"grey"}
          returnKeyType={"send"}
          autoCorrect={true}
          onSubmitEditing={page.onEmailProcess}
          onChangeText={(value) => {page.email = value}}
        />

        <Button style={styles.buttonDesign} onPress={page.onEmailProcess}>
          <Text style={styles.buttonText}>Send</Text>
          <Icon name="check-circle" style={styles.icon} />
        </Button>

        <View style={{height:100}}></View>

      </ScrollView>

    </KeyboardAvoidingView>

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  body:{
    padding:20,
  },
  textInputDesign:{
    marginTop:-2,
    borderWidth:1,
    marginBottom:10,
    borderColor:'#ddd',
    paddingVertical:10,
    paddingHorizontal:10,
    backgroundColor:'#ffffff',
  },
  buttonDesign:{
    height:40,
    marginTop:15,
    borderRadius:5,
    borderColor:'#ddd',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#78e08f',
  },
  icon:{
    right:10,
    fontSize:22,
    color:'white',
    position:'absolute',
  },
  buttonText:{
    color:'white',
  },
  question:{
    borderWidth:1,
    paddingVertical:10,
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:10,
    borderColor:'#fa6304',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    backgroundColor:'#fa6304',
    justifyContent:'space-between',
  },
  questionText:{
    fontSize:15,
    color:'white',
    fontWeight:'bold',
  },
});

export default page;
