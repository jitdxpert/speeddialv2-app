import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Button,
  ScrollView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/SpeedDial';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/SpeedDial';

const DayCircle = JSX((props) => {

  let day = props.day;
  let key = props.index;
  return (
    <Button
      style={[day.text ? calendar.day : calendar.dayTransperent, day.today ? calendar.toDay : null, day.hasItem ? calendar.hasItem : null, page.selected == key ? calendar.selected : null]}
      onPress={()=>{page.onPressCalendarDay(day, key)}}>
      <Text style={[calendar.dayText, day.today ? calendar.toDayText : null, day.hasItem ? calendar.hasItemText : null, page.selected == key ? calendar.selectedText : null]}>{day.text}</Text>
    </Button>
  );

});


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={false}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      onPressPrev={page.prevMonth}
      onPressNext={page.nextMonth}
      month={page.month}
      year={page.year}
    />

    <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

      <View style={calendar.container}>

        <View style={calendar.week}>
          {page.week.map((day, i) => (
            <View key={i} style={calendar.dayHeader}>
              <Text allowFontScaling={false} style={[calendar.dayText, calendar.headerText]}>{day}</Text>
            </View>
          ))}
        </View>

        {page.calendar.map((week, i) => (
          <View key={i} style={calendar.week}>
            {week.map((day, j) => (
              <DayCircle key={i+'-'+j} day={day} index={i+'-'+j} />
            ))}
          </View>
        ))}

      </View>

      <Button style={styles.buttonBody} onPress={page.onPressJournal}>
        <Text style={styles.buttonTextSign}>Speed Dial</Text>
        <View style={styles.iconPositionSign}>
          <Icon name="ion-ios-compose-outline" style={styles.icon}/>
        </View>
      </Button>

      <Button style={styles.buttonBodyLast} onPress={page.onPressHowToUse}>
        <Text style={styles.buttonLastText}>HOW TO USE GUIDE</Text>
      </Button>

    </ScrollView>

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  buttonBody:{
    height:50,
    borderRadius:5,
    flexDirection:'row',
    alignItems:'center',
    marginHorizontal:20,
    backgroundColor:'#78e08f',
  },
  buttonTextSign:{
    fontSize:16,
    width:'100%',
    color:'white',
    fontWeight:'bold',
    textAlign:'center',
  },
  iconPositionSign:{
    right:20,
    position:'absolute',
  },
  buttonBodyLast:{
    height:50,
    marginTop:40,
    flexDirection:'row',
    marginHorizontal:20,
    justifyContent:'center',
  },
  buttonLastText:{
    fontSize:14,
    color:'blue',
    fontWeight:'bold',
  },
  icon:{
    fontSize:22,
    color:'white',
  },
});

const calendar = Style({
  container:{
    zIndex:1,
    paddingVertical:10,
    position:'relative',
  },
  week:{
    paddingVertical:10,
    flexDirection:'row',
    paddingHorizontal:20,
    justifyContent:'space-around',
  },
  dayHeader:{
    flex:1,
    alignItems:'center',
  },
  headerText:{
    fontSize:18,
    fontWeight:'bold',
  },
  dayText:{
    fontSize:15,
    color:'#5c596d',
  },
  toDayText:{
    fontSize:15,
    color:'#e05729',
  },
  day:{
    width:40,
    height:40,
    elevation:1,
    marginTop:10,
    borderRadius:40/2,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'white',
  },
  dayTransperent:{
    width:40,
    height:40,
    marginTop:10,
    borderRadius:40/2,
    alignItems:'center',
    justifyContent:'center',
  },
  toDay:{
    borderWidth:1,
    borderColor:'#e05729',
  },
  selected:{
    backgroundColor:'#5c596d',
  },
  selectedText:{
    fontSize:15,
    color:'#fff',
  },
  hasItem:{
    backgroundColor:'red',
  },
  hasItemText:{
    fontSize:15,
    color:'#fff',
  },
});

export default page;
