import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Video,
  Button,
  Spinner,
  ScrollView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Video';
import Empty from 'Shared/Empty';
import Loader from 'Shared/Loader';
import Footer from 'Shared/Footer';
import Header from 'Shared/Headers/Common';

const isVideoLoading = (loading) => {
  return loading ? <Spinner style={styles.spinner} size="small" color="#fff" /> : null;
};

const VideoLoader = JSX((props) => (
  isVideoLoading(props.show)
));

const videoCard = (videos) => {
  if(!videos.length){
    return <Empty icon={page.icon} message={page.message} />;
  }

  return videos.map((video, i) => (
    <View key={i} style={styles.videoScreenLayout}>
      <View style={styles.videoTitle}>
        <Text style={styles.videoTitleText}>{video.Video_Title}</Text>
      </View>
      <View style={styles.videoContainer}>
        <VideoLoader show={video.loading} />
        <Video
          source={{uri: encodeURI(page.baseurl+'/uploads/videos/'+video.Video_File)}}
          style={styles.videoScreen}
          paused={video.paused}
          resizeMode={page.resizeMode}
          onEnd={() => {page.onEnd(video)}}
          onLoadStart={() => {page.onLoadStart(video)}}
          onLoad={(data) => {page.onLoad(data, video)}}
          onProgress={(data) => {page.onProgress(data, video)}}
          repeat={true}
        />
      </View>
      <View style={styles.videoBar}>
        <Button onPress={() => {page.onPlay(video, i)}} disabled={video.loading}>
          <Icon style={styles.icon} name={video.playPauseIcon} />
        </Button>

        <Text style={styles.videoBarText}>{video.currentTime}</Text>
        <Text style={styles.videoBarText1}>{video.duration}</Text>
      </View>
    </View>
  ));
};

page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={false}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Video"
    />

    <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

      {videoCard(page.videos)}

    </ScrollView>

    <Footer active={0} onPress={page.onMediaTabChange} />

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  body:{
    paddingHorizontal:20,
    backgroundColor:'#f5f5f5',
  },
  videoScreenLayout:{
    width:'100%',
    marginVertical:15,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#E5E5E5'
  },
  videoContainer:{
    width:'100%',
    position:'relative',
  },
  spinner:{
    zIndex:99,
    width:'100%',
    height:'100%',
    alignSelf:'center',
    position:'absolute',
    backgroundColor:'#ffffff50'
  },
  videoScreen:{
    height:250,
    width:'100%',
    borderWidth:1,
    borderColor:'#fff',
  },
  videoBar:{
    height:50,
    elevation:1,
    width:'100%',
    paddingRight:20,
    borderColor:'#666',
    flexDirection:'row',
    borderTopWidth:0.25,
    alignItems:'center',
    backgroundColor:'black',
    justifyContent:'space-between',
  },
  videoBarText:{
    fontSize:16,
    color:'white',
  },
  videoBarText1:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
  },
  videoTitle:{
    elevation:1,
    width:'100%',
    paddingVertical:15,
    alignItems:'center',
    flexDirection:'row',
    paddingHorizontal:20,
    backgroundColor:'white',
  },
  videoTitleText:{
    fontSize:16,
    color:'black',
    fontWeight:'bold',
  },
  icon:{
    fontSize:18,
    color:'white',
    paddingVertical:10,
    paddingHorizontal:20,
  },
});

export default page;
