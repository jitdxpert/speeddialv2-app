import React from 'react';
import { JSX, Style, Platform } from 'Modules';
import {
  Text,
  View,
  Icon,
  Button,
  TextInput,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Consultation';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Common';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={false}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Free BIZ Consultation"
    />

    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : ''}>

      <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

        <View style={styles.question}>
          <Text style={styles.questionText}>Subject</Text>
        </View>
        <TextInput style={[styles.textInputDesign, styles.height80]}
          ref={(input) => {page.subjectInput = input}}
          multiline={true}
          placeholder="SpeedDial Free BIZ Consultancy"
          underlineColorAndroid={"transparent"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          onSubmitEditing={() => {page.queryInput.focus()}}
          onChangeText={(value) => {page.subject = value}}
          value={page.subject}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>What is your biggest issue with your BIZ now?</Text>
        </View>
        <TextInput style={[styles.textInputDesign, styles.height100]}
          ref={(input) => {page.queryInput = input}}
          placeholder="What is your biggest issue with your BIZ now?"
          underlineColorAndroid={"transparent"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          numberOfLines={4}
          multiline={true}
          onSubmitEditing={() => {page.stateInput.focus()}}
          onChangeText={(value) => {page.query = value}}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>Country or State</Text>
        </View>
        <TextInput style={[styles.textInputDesign, styles.height50]}
          ref={(input) => {page.stateInput = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Country or State"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          autoCapitalize={"words"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={true}
          onSubmitEditing={() => {page.mobileInput.focus()}}
          onChangeText={(value) => {page.state = value}}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>Mobile Number</Text>
        </View>
        <TextInput style={[styles.textInputDesign, styles.height50]}
          ref={(input) => {page.mobileInput = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Mobile Number"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          keyboardType={"numeric"}
          returnKeyType={"next"}
          blurOnSubmit={false}
          autoCorrect={false}
          onSubmitEditing={() => {page.skypeInput.focus()}}
          onChangeText={(value) => {page.mobile = value}}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>Skype ID</Text>
        </View>
        <TextInput style={[styles.textInputDesign, styles.height50]}
          ref={(input) => {page.skypeInput = input}}
          underlineColorAndroid={"transparent"}
          placeholderTextColor={"grey"}
          textAlignVertical={"top"}
          placeholder={"Skype ID"}
          autoCapitalize={"none"}
          returnKeyType={"send"}
          autoCorrect={false}
          onSubmitEditing={page.onConsultationProcess}
          onChangeText={(value) => {page.skype = value}}
        />

        <Button style={styles.buttonDesign} onPress={page.onConsultationProcess}>
          <Text style={styles.buttonText}>Submit</Text>
          <Icon name="check-circle" style={styles.icon} />
        </Button>

        <View style={{height:100}}></View>

      </ScrollView>

    </KeyboardAvoidingView>

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  body:{
    padding:20,
  },
  textInputDesign:{
    marginTop:-2,
    borderWidth:1,
    marginBottom:10,
    borderColor:'#ddd',
    paddingVertical:10,
    paddingHorizontal:10,
    backgroundColor:'white',
    borderBottomLeftRadius:5,
    borderBottomRightRadius:5,
  },
  height50:{
    minHeight:50,
  },
  height80:{
    minHeight:80,
  },
  height100:{
    minHeight:100,
  },
  buttonDesign:{
    height:40,
    marginTop:20,
    borderRadius:5,
    borderColor:'#ddd',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    justifyContent:'center',
    backgroundColor:'#78e08f',
  },
  icon:{
    right:10,
    fontSize:16,
    color:'white',
    position:'absolute',
  },
  buttonText:{
    fontSize:16,
    color:'white',
  },
  question:{
    borderWidth:1,
    paddingVertical:10,
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:10,
    borderColor:'#fa6304',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    backgroundColor:'#fa6304',
    justifyContent:'space-between',
  },
  questionText:{
    fontSize:15,
    color:'white',
    fontWeight:'bold',
  },
});

export default page;
