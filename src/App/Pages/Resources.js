import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Button,
  ScrollView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Resources';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Common';


const eventButtonText = (event) => {
  if(event.Button_Text){
    return <Text style={styles.buttonText}>{event.Button_Text}</Text>;
  }else{
    if(event.Event_Price){
      return <Text style={styles.buttonText}>{page.buyBtn} for ${event.Event_Price}</Text>;
    }else{
      return <Text style={styles.buttonText}>{page.freeBtn}</Text>;
    }
  }
};

const eventCard = (event) => (
  <View style={styles.row}>
    <View style={styles.listItemTop}>
      <Text style={styles.listTextTop}>{event.Event_Name}</Text>
    </View>
    <View style={styles.listItem}>
      <Text style={styles.listText}>{event.Event_Desc}</Text>
    </View>
    <View style={styles.listItemLast}>
      <Text style={styles.listText}>{event.Event_Venue}</Text>
      <Text style={styles.listText}>{event.Event_Time}</Text>
    </View>
    <Button style={[styles.buttonDesign,{backgroundColor:page.buttonColor}]} onPress={() => {page.onPressEventLink(event)}}>
      {eventButtonText(event)}
    </Button>
  </View>
);

const productButtonText = (event) => {
  if(event.Button_Text){
    return <Text style={styles.buttonText}>{event.Button_Text}</Text>;
  }else{
    if(event.Product_Spl_Price){
      return <Text style={styles.buttonText}>{page.buyBtn} for ${event.Product_Spl_Price}</Text>;
    }else{
      return <Text style={styles.buttonText}>{page.freeBtn}</Text>;
    }
  }
};

const productCard = (event) => (
  <View style={styles.row}>
    <View style={styles.listItemTop}>
      <Text style={styles.listTextTop}>{event.Product_Name}</Text>
    </View>
    <View style={styles.listItem}>
      <Text style={styles.listText}>{event.Product_Desc}</Text>
    </View>
    <Button style={[styles.buttonDesign,{backgroundColor:page.buttonColor}]} onPress={() => {page.onPressEventLink(event)}}>
      {productButtonText(event)}
    </Button>
  </View>
);

page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={false}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Resources"
    />

    <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

      {page.events.map((event, e) => (
        <View key={e}>
          {event.From==='event'?eventCard(event):productCard(event)}
        </View>
      ))}

    </ScrollView>

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  body:{
    padding:20,
  },
  row:{
    marginBottom:20,
  },
  buttonDesign:{
    height:40,
    marginTop:-1,
    borderColor:'#ddd',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    justifyContent:'center',
    borderBottomLeftRadius:5,
    backgroundColor:'#fa6304',
    borderBottomRightRadius:5,
  },
  buttonText:{
    color:'white',
  },
  listItem:{
    borderWidth:1,
    paddingVertical:10,
    borderColor:'#ddd',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    backgroundColor:'white',
    justifyContent:'space-between',
  },
  listItemTop:{
    paddingVertical:10,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    backgroundColor:'#2d173c',
    justifyContent:'space-between',
  },
  listItemLast:{
    marginTop:-1,
    borderWidth:1,
    borderColor:'#ddd',
    paddingVertical:10,
    paddingHorizontal:20,
    justifyContent:'center',
    backgroundColor:'white',
  },
  listText:{
    color:'#696969',
  },
  listTextTop:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
  },
});

export default page;
