import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Button,
  Switch,
  TextInput,
  DatePicker,
  ScrollView,
  ActionSheet,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Profile';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Profile';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={false}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      onPressImage={page.onPhotoUpload}
      title={page.user.User_FullName}
      image={page.image}
    />

    <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

      <View style={styles.listItem}>
        <View style={styles.column}>
          <Icon style={styles.icon} name="mail" />
          <Text style={styles.inpText}>{page.user.User_Email}</Text>
        </View>
      </View>

      <View style={styles.listItem}>
        <View style={styles.column}>
          <Icon style={styles.icon} name="clock" />
          <Text style={styles.inpText}>Remind For Journal:</Text>
        </View>
        <Switch onValueChange={page.onReminderSwitch} value={page.reminderSwitch} />
      </View>

      <View style={styles.listItem}>
        <View style={styles.column}>
          <Icon style={styles.icon} name="clock" />
          <Text style={styles.inpText}>Reminder At:</Text>
        </View>
        <Button onPress={page.onPressReminderTime}>
          <Text style={styles.inpTextTime}>{page.reminderTime}</Text>
        </Button>
      </View>

      {page.user.User_Password ? (
        <View style={styles.listItem}>
          <Button onPress={page.onPressChangePassword}>
            <View style={styles.column}>
              <Icon style={styles.icon} name="lock" />
              <Text style={styles.inpText}>Change Password</Text>
            </View>
          </Button>
        </View>
      ) : null}

    </ScrollView>

    <DatePicker
      show={page.showTimePicker}
      mode={'time'}
      format={'HH:mm'}
      onSelect={page.onReminderSelect}
      onClose={() => {page.showTimePicker = false}}
    />

    <Loader show={app.loading} />

    <ActionSheet
      ref={page.onActionSheetInit}
      title={'Choose Profile Photo'}
      options={['Open Camera', 'Open Library', 'Cancel']}
      cancelButtonIndex={2}
      destructiveButtonIndex={2}
      onPress={page.onActionSheetPress}
    />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
  },
  body:{
    flex:1,
    paddingVertical:15,
    paddingHorizontal:20,
    backgroundColor:'#f5f5f5',
  },
  listItem:{
    height:50,
    elevation:1,
    marginTop:20,
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    backgroundColor:'white',
    justifyContent:'space-between',
  },
  column:{
    alignItems:'center',
    flexDirection:'row',
  },
  inpText:{
    marginLeft:20,
  },
  inpTextTime:{
    color:'black',
    fontWeight:'bold',
  },
  icon:{
    fontSize:26,
    color:'black',
  },
});

export default page;
