import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Image,
  Button,
  ScrollView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/SideBar';


page.template = JSX(() => (

  <View style={styles.container}>

    <View style={styles.body}>

      <Button style={styles.profileContainer} onPress={page.onPressProfile}>
        <Image source={page.image} style={styles.profileImage}/>
        <Text allowFontScaling={false} style={styles.textName}>{page.user.User_FullName}</Text>
      </Button>

      <ScrollView contentContainerStyle={styles.menu} keyboardShouldPersistTaps="handled">

        <Button style={styles.button} onPress={page.onPressSpeedDial}>
          <Icon style={styles.icon} name="book" />
          <View style={styles.buttonTextDesign}>
            <Text allowFontScaling={false} style={styles.buttonText}>Speed Dial</Text>
          </View>
        </Button>

        <Button style={styles.button} onPress={page.onPressStatistics}>
          <Icon style={styles.icon} name="statistics" />
          <View style={styles.buttonTextDesign1}>
            <Text allowFontScaling={false} style={styles.buttonText}>Statistics</Text>
          </View>
        </Button>

        <Button style={styles.button} onPress={page.onPressHowToUse}>
          <Icon style={styles.icon} name="question" />
          <View style={styles.buttonTextDesign}>
            <Text allowFontScaling={false} style={styles.buttonText}>How To Use</Text>
          </View>
        </Button>

        <Button style={styles.button} onPress={page.onPressProfile}>
          <Icon style={styles.icon} name="profile" />
          <View style={styles.buttonTextDesignProfile}>
            <Text allowFontScaling={false} style={styles.buttonText}>Profile</Text>
          </View>
        </Button>

        <Button style={styles.button} onPress={page.onPressResources}>
          <Icon style={styles.icon} name="resource" />
          <View style={styles.buttonTextDesign1}>
            <Text allowFontScaling={false} style={styles.buttonText}>Resources</Text>
          </View>
        </Button>

        <Button style={styles.button} onPress={page.onPressMedia}>
          <Icon style={styles.icon} name="media" />
          <View style={styles.buttonTextDesign}>
            <Text allowFontScaling={false} style={styles.buttonText}>Media</Text>
          </View>
        </Button>

        <Button style={styles.button} onPress={page.onPressConsultation}>
          <Icon style={styles.icon} name="phone" />
          <View style={styles.buttonTextDesign}>
            <Text allowFontScaling={false}  style={styles.buttonText}>Free BIZ Consultation</Text>
          </View>
        </Button>

        <Button style={styles.button} onPress={page.onPressAbout}>
          <Icon style={styles.icon} name="info" />
          <View style={styles.buttonTextDesign}>
            <Text allowFontScaling={false} style={styles.buttonText}>About</Text>
          </View>
        </Button>

        <Button style={styles.button} onPress={page.onPressHelp}>
          <Icon style={styles.icon} name="cog" />
          <View style={styles.buttonTextDesign1}>
            <Text allowFontScaling={false} style={styles.buttonText}>Help</Text>
          </View>
        </Button>

        <Button style={[styles.button, styles.logout]} onPress={page.onPressLogout}>
          <Icon style={styles.icon} name="logout" />
          <View style={styles.buttonTextDesign1}>
            <Text allowFontScaling={false} style={styles.buttonText}>Logout</Text>
          </View>
        </Button>

      </ScrollView>

    </View>

    <Button style={styles.overlay} onPress={page.onPressCloseNavbar}></Button>

  </View>

));


const styles = Style({
  container:{
    flex:1,
    flexDirection:'row',
  },
  body:{
    width:'75%',
    backgroundColor:'#e05729',
  },
  overlay:{
    width:'25%',
    height:'100%',
  },
  profileContainer:{
    paddingBottom:20,
    borderColor:'#ffffff70',
    alignItems:'center',
    borderBottomWidth:0.5,
  },
  profileImage:{
    width:125,
    height:125,
    marginTop:40,
    borderWidth:4,
    borderRadius:125/2,
    borderColor:'#ffffff70',
  },
  textName:{
    fontSize:25,
    color:'#fff',
    marginTop:15,
    fontWeight:'bold',
  },
  button:{
    height:50,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
  },
  logout:{
    marginBottom:30,
  },
  icon:{
    fontSize:30,
    color:'white',
  },
  buttonText:{
    fontSize:18,
    color:'white',
    fontWeight:'bold',
  },
  buttonTextDesign:{
    marginLeft:20,
  },
  buttonTextDesign1:{
    marginLeft:15,
  },
  buttonTextDesignProfile:{
    marginLeft:25,
  },
});

export default page;
