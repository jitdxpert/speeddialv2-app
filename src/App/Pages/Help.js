import React from 'react';
import { JSX, Style, Platform } from 'Modules';
import {
  Text,
  View,
  Icon,
  Button,
  TextInput,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Help';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Common';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={false}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Help"
    />

    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : ''}>

      <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

        <View style={styles.question}>
          <Text style={styles.questionText}>Briefly explain issue and we'll get back to you in 48 hours or less!</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.concernInput = input}}
          multiline={true}
          numberOfLines={8}
          textAlignVertical={"top"}
          underlineColorAndroid={"transparent"}
          placeholder="Briefly explain issue and we'll get back to you in 48 hours or less!"
          placeholderTextColor={"grey"}
          returnKeyType={"send"}
          autoCorrect={true}
          onSubmitEditing={page.onHelpProcess}
          onChangeText={(value) => {page.concern = value}}
          value={page.concern}
        />

        <Button style={styles.buttonDesign} onPress={page.onHelpProcess}>
          <Text style={styles.buttonText}>Submit</Text>
          <Icon name="settings" style={styles.icon}/>
        </Button>

        <View style={{height:100}}></View>

      </ScrollView>

    </KeyboardAvoidingView>

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  body:{
    padding:20,
  },
  textInputDesign:{
    marginTop:-2,
    borderWidth:1,
    minHeight:200,
    marginBottom:10,
    borderColor:'#ddd',
    paddingVertical:10,
    paddingHorizontal:10,
    backgroundColor:'white',
    borderBottomLeftRadius:5,
    borderBottomRightRadius:5,
  },
  buttonDesign:{
    height:40,
    marginTop:20,
    borderRadius:5,
    borderColor:'#ddd',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    justifyContent:'center',
    backgroundColor:'#78e08f',
  },
  icon:{
    right:10,
    fontSize:16,
    color:'white',
    position:'absolute',
  },
  buttonText:{
    fontSize:16,
    color:'white',
  },
  question:{
    borderWidth:1,
    paddingVertical:10,
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:10,
    borderColor:'#fa6304',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    backgroundColor:'#fa6304',
    justifyContent:'space-between',
  },
  questionText:{
    fontSize:15,
    color:'white',
    fontWeight:'bold',
  },
});

export default page;
