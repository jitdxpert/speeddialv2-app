import React from 'react';
import { JSX, Style, Screen } from 'Modules';
import {
  View,
  Image,
  StatusBar,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Splash';


page.template = JSX(() => (

  <View style={styles.container}>

    <StatusBar
      backgroundColor="#e05729"
      barStyle="light-content"
    />

    <Image style={styles.imageStyle} source={page.splashUri} />

  </View>

));


const styles = Style({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#e05729',
  },
  imageStyle:{
    width:Screen.width-20,
    height:Screen.height-20,
  },
});

export default page;
