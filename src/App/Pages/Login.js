import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Image,
  Button,
  StatusBar,
  TextInput,
  ScrollView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/Login';
import Loader from 'Shared/Loader';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <StatusBar
      backgroundColor="white"
      barStyle="dark-content"
    />

    <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

      <Image style={styles.imgStyle} source={page.bannerUri}/>

      <Button style={styles.buttonBody} onPress={page.onPressFbLogin}>
        <Text style={styles.buttonTextFacebook}>Login with Facebook</Text>
        <View style={styles.iconPositionFacebook}>
          <Icon name="facebook" style={styles.icon}/>
        </View>
      </Button>

      <Button style={styles.buttonBodyMiddle} onPress={page.onPressGpLogin}>
        <Text style={styles.buttonTextGoogle}>Login with Google</Text>
        <View style={styles.iconPositionGoogle}>
          <Icon name="google" style={styles.icon}/>
        </View>
      </Button>

      <Text style={styles.middleLine}>OR</Text>

      <Button style={styles.buttonBodyLast} onPress={page.onPressLogin}>
        <Text style={styles.buttonTextRegisterEmail}>Login with Email</Text>
        <View style={styles.iconPositionRegisterEmail}>
          <Icon name="mail-dark" style={styles.icon}/>
        </View>
      </Button>

      <Button style={styles.backtoHomeButton} onPress={page.onPressBackToHome}>
        <Icon name="arrow-left" style={styles.backIcon}/>
        <Text style={styles.buttonTextBackToHome}>Back to Home</Text>
      </Button>

    </ScrollView>

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
  },
  body:{
    paddingVertical:12,
    paddingBottom:50,
    alignItems:'center',
  },
  middleLine:{
    fontSize:16,
    color:'black',
    marginVertical:20,
  },
  buttonTextBackToHome:{
    fontSize:12,
    color:'grey',
  },
  iconPositionGoogle:{
    right:10,
    position:'absolute',
  },
  iconPositionFacebook:{
    right:10,
    position:'absolute',
  },
  iconPositionRegisterEmail:{
    right:10,
    position:'absolute',
  },
  icon:{
    fontSize: 22,
    color:'white',
  },
  backIcon:{
    fontSize:20,
    color:'grey',
  },
  imgStyle:{
    width:320,
    height:230,
    marginTop:20,
    marginBottom:20,
  },
  buttonBody:{
    height:50,
    width:'90%',
    marginTop:10,
    borderRadius:5,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#3B5998',
  },
  buttonBodyMiddle:{
    height:50,
    width:'90%',
    marginTop:10,
    borderRadius:5,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#dc4538',
  },
  buttonBodyLast:{
    height:50,
    width:'90%',
    marginTop:10,
    borderRadius:5,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#636e72',
  },
  backtoHomeButton:{
    height:30,
    width:'30%',
    marginTop:15,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
  },
  buttonTextFacebook:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
    position:'absolute',
  },
  buttonTextGoogle:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
    position:'absolute',
  },
  buttonTextRegisterEmail:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
    position:'absolute',
  },
});

export default page;
