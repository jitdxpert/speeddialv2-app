import React from 'react';
import { JSX, Style, Platform } from 'Modules';
import {
  View,
  Text,
  Icon,
  Button,
  TextInput,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/ChangePassword';
import Loader from 'Shared/Loader';
import Header from 'Shared/Headers/Common';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <Header
      hasBack={true}
      onPressNav={page.onPressOpenNavbar}
      onPressBack={page.onPressBack}
      title="Change Password"
    />

    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : ''}>

      <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

        <View style={styles.question}>
          <Text style={styles.questionText}>Current Password:</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.curPassInput = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Enter current password"}
          placeholderTextColor={"grey"}
          returnKeyType={"next"}
          secureTextEntry={true}
          autoCorrect={false}
          blurOnSubmit={false}
          onSubmitEditing={() => {page.newPassInput.focus()}}
          onChangeText={(value) => {page.curPass = value}}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>New Password:</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.newPassInput = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Enter new password"}
          placeholderTextColor={"grey"}
          returnKeyType={"next"}
          secureTextEntry={true}
          autoCorrect={false}
          blurOnSubmit={false}
          onSubmitEditing={() => {page.conPassInput.focus()}}
          onChangeText={(value) => {page.newPass = value}}
        />

        <View style={styles.question}>
          <Text style={styles.questionText}>Confirm Password:</Text>
        </View>
        <TextInput style={styles.textInputDesign}
          ref={(input) => {page.conPassInput = input}}
          underlineColorAndroid={"transparent"}
          placeholder={"Confirm new password"}
          placeholderTextColor={"grey"}
          secureTextEntry={true}
          returnKeyType={"done"}
          autoCorrect={false}
          onSubmitEditing={page.onChangePassProcess}
          onChangeText={(value) => {page.conPass = value}}
        />

        <Button style={styles.buttonDesign} onPress={page.onChangePassProcess}>
          <Text style={styles.buttonText}>Submit</Text>
          <Icon name="lock" style={styles.icon} />
        </Button>

        <View style={{height:100}}></View>

      </ScrollView>

    </KeyboardAvoidingView>

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    backgroundColor:'#f5f5f5',
  },
  body:{
    padding:20,
  },
  textInputDesign:{
    marginTop:-2,
    borderWidth:1,
    marginBottom:10,
    borderColor:'#ddd',
    paddingHorizontal:10,
    backgroundColor:'white',
    borderBottomLeftRadius:5,
    borderBottomRightRadius:5,
  },
  buttonDesign:{
    height:40,
    marginTop:20,
    borderRadius:5,
    borderColor:'#ddd',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:20,
    justifyContent:'center',
    backgroundColor:'#78e08f',
  },
  icon:{
    right:10,
    fontSize:20,
    color:'white',
    position:'absolute',
  },
  buttonText:{
    fontSize:16,
    color:'white',
  },
  question:{
    borderWidth:1,
    paddingVertical:10,
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:10,
    borderColor:'#fa6304',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    backgroundColor:'#fa6304',
    justifyContent:'space-between',
  },
  questionText:{
    fontSize:15,
    color:'white',
    fontWeight:'bold',
  },
});

export default page;
