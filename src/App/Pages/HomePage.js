import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Image,
  Button,
  StatusBar,
  ScrollView,
  SafeAreaView,
} from 'Widgets';
import app from 'Logic/App';
import page from 'Logic/HomePage';
import Loader from 'Shared/Loader';


page.template = JSX(() => (

  <SafeAreaView style={styles.container}>

    <StatusBar
      backgroundColor="white"
      barStyle="dark-content"
    />

    <ScrollView contentContainerStyle={styles.body} keyboardShouldPersistTaps="handled">

      <Image style={styles.imageStyle} source={page.bannerUri}/>

      <View style={styles.textContainer}>
        <Text style={styles.headingFirstLine}>Speed Dial the </Text>
        <Text style={styles.headingSecondLine}>Universe Journal </Text>
        <Text style={styles.subheadingLineOne}>Have you ever wished there was a daily  </Text>
        <Text style={styles.subheadingLineTwo}>practice that would set you up for extreme </Text>
        <Text style={styles.subheadingLineTwo}>success and abundance?</Text>
      </View>

      <Button style={styles.buttonBodySignIn} onPress={page.onPressSignin}>
        <Text style={styles.buttonTextSign}>Sign in</Text>
        <View style={styles.iconPositionSign}>
          <Icon name="login" style={styles.icon}/>
        </View>
      </Button>

      <Button style={styles.buttonBodySignUp} onPress={page.onPressSignup}>
        <Text style={styles.buttonTextAccount}>Create an account</Text>
        <View style={styles.iconPositionAccount}>
          <Icon name="user-add" style={styles.icon}/>
        </View>
      </Button>

    </ScrollView>

    <Loader show={app.loading} />

  </SafeAreaView>

));


const styles = Style({
  container:{
    flex:1,
    justifyContent:'space-around'
  },
  body:{
    paddingVertical:12,
    alignItems:'center',
    paddingBottom:50
  },
  textContainer:{
    flex:0.8,
    alignItems:'center',
    paddingHorizontal:5,
  },
  heading:{
    fontSize:32,
  },
  headingFirstLine:{
    fontSize:28,
    color:'#3c1f4d',
    fontWeight:'bold',
    paddingHorizontal:10,
  },
  headingSecondLine:{
    fontSize:28,
    color:'#3c1f4d',
    fontWeight:'bold',
    paddingHorizontal:5,
  },
  subheadingLineOne:{
    fontSize:16,
    marginTop:20,
    paddingHorizontal:5,
  },
  subheadingLineTwo:{
    fontSize:16,
  },
  subheadingLineThree:{
    fontSize:18,
  },
  iconPositionSign:{
    right:10,
    position:'absolute',
  },
  iconPositionAccount:{
    right:10,
    position:'absolute',
  },
  icon:{
    fontSize:22,
    color:'white',
  },
  imageStyle:{
    width:320,
    height:200,
  },
  buttonBodySignIn:{
    height:50,
    width:'90%',
    marginTop:30,
    borderRadius:5,
    borderColor:'#fff',
    alignItems:'center',
    flexDirection:'row',
    paddingHorizontal:10,
    backgroundColor:'blue',
    justifyContent:'center',
    backgroundColor:'#fa6304',
  },
  buttonBodySignUp:{
    height:50,
    width:'90%',
    marginTop:20,
    borderRadius:5,
    borderColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:10,
    backgroundColor:'blue',
    justifyContent:'center',
    backgroundColor:'#3c1f4d',
  },
  buttonTextSign:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
    position:'absolute',
  },
  buttonTextAccount:{
    fontSize:16,
    color:'white',
    fontWeight:'bold',
    position:'absolute',
  },
});

export default page;
