import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  View,
  Icon,
  Button
} from 'Widgets';


const isActive = (item, active) => {
  return item == active ? {color:'#e05729'} : {};
};

const Footer = JSX((props) => (

  <View style={styles.footer}>

    <Button onPress={()=>{props.onPress(0)}} style={styles.button}>
      <Icon name="videocam" style={[styles.icon, isActive(0, props.active)]} />
      <Text style={[styles.text, isActive(0, props.active)]}>Video</Text>
    </Button>

    <Button onPress={()=>{props.onPress(1)}} style={styles.button}>
      <Icon name="mic" style={[styles.icon, isActive(1, props.active)]} />
      <Text style={[styles.text, isActive(1, props.active)]}>Audio</Text>
    </Button>

  </View>

));


const styles = Style({
  footer:{
    borderTopWidth:0.5,
    flexDirection:'row',
    borderColor:'#e3e3e3',
    backgroundColor:'white',
    justifyContent:'space-around',
  },
  button:{
    paddingVertical:14,
    alignItems:'center',
    paddingHorizontal:30,
    justifyContent:'center',
  },
  icon:{
    fontSize:28,
    color:'#555',
  },
  text:{
    fontSize:14,
    color:'#555',
  },
});

export default Footer;
