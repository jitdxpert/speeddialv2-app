import React from 'react';
import { JSX, Style, Screen } from 'Modules';
import {
  Text,
  View,
  Icon,
} from 'Widgets';
import app from 'Logic/App';


const Empty = JSX((props) => (

  <View style={styles.container}>
    <Icon style={styles.icon} name={props.icon} />
    <Text style={styles.text}>{props.message}</Text>
  </View>

));


const styles = Style({
  container:{
    alignItems:'center',
    justifyContent:'center',
    height:Screen.height*0.6,
  },
  icon:{
    fontSize:122,
    color:'#bbb',
  },
  text: {
    fontSize:22,
    color:'#999',
  },
  spinner:{
    top:40,
    zIndex:2,
    position:'absolute',
  },
});

export default Empty;
