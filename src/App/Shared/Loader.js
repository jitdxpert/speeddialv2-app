import React from 'react';
import { JSX, Style } from 'Modules';
import {
  View,
  Spinner,
} from 'Widgets';
import app from 'Logic/App';


const isLoading = (loading) => {
  return loading ? <Spinner style={styles.spinner} size="large" color="#666" /> : null;
};

const Loader = JSX((props) => (

  <View style={props.show ? styles.container : ''}>
    {isLoading(props.show)}
  </View>

));


const styles = Style({
  container:{
    top:0,
    left:0,
    right:0,
    bottom:0,
    zIndex:9,
    height:'100%',
    position:'absolute',
    alignItems:'center',
    justifyContent:'center',
  },
  hidden:{
    height:'auto',
    position:'relative',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'transparent',
  },
  spinner:{
    zIndex:99,
    alignSelf:'center',
    position:'absolute',
  },
});

export default Loader;
