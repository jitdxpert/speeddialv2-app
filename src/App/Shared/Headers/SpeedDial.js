import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  Icon,
  View,
  Button,
  StatusBar,
} from 'Widgets';
import app from 'Logic/App';


const Header = JSX((props) => (

  <View style={styles.container}>
    <StatusBar
      backgroundColor="#2d173c"
      barStyle="light-content"
    />
    <View style={styles.statusBarOverlay} />

    <View style={styles.header}>

      <Button onPress={props.onPressNav} style={styles.navButton}>
        <Icon name="menu" style={styles.icon}/>
      </Button>

      <Button onPress={props.onPressPrev} style={styles.button}>
        <Icon name="angle-left" style={styles.buttonText}/>
      </Button>

      <View style={{
        width:150,
        flexDirection:'row',
        justifyContent: 'center'
      }}>
      <Text allowFontScaling={false} style={styles.month}>{props.month}</Text>
      <Text allowFontScaling={false} style={styles.year}>{props.year}</Text>
      </View>

      <Button onPress={props.onPressNext} style={styles.button}>
        <Icon name="angle-right" style={styles.buttonText}/>
      </Button>

    </View>
  </View>

));


const styles = Style({
  container:{
    zIndex:9,
    position:'relative',
  },
  statusBarOverlay:{
    top:-60,
    height:60,
    width:'100%',
    position:'absolute',
    backgroundColor:'#2d173c'
  },
  header:{
    height:50,
    alignItems:'center',
    flexDirection:'row',
    borderBottomWidth:0.5,
    borderColor:'#e3e3e3',
    justifyContent:'center',
    backgroundColor:'#2d173c',
  },
  navButton:{
    left:0,
    padding:20,
    position:'absolute',
  },
  icon:{
    fontSize:24,
    color:'#fff',
  },
  button:{
    paddingHorizontal:5,
  },
  buttonText:{
    fontSize:18,
    color:'#fff',
    padding:8,
  },
  month:{
    fontSize:18,
    color:'#fff',
  },
  year:{
    fontSize:18,
    color:'#fff',
    marginLeft: 6,
  },
});

export default Header;
