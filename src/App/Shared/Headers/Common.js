import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  Icon,
  View,
  Button,
  StatusBar,
} from 'Widgets';


const Header = JSX((props) => (

  <View style={styles.container}>
    <StatusBar
      backgroundColor="#2d173c"
      barStyle="light-content"
    />
    <View style={styles.statusBarOverlay} />

    {props.hasBack ? (
      <Button onPress={props.onPressBack} style={styles.menu}>
        <Icon name="arrow-left" style={styles.icon} />
      </Button>
    ) : (
      <Button onPress={props.onPressNav} style={styles.menu}>
        <Icon name="menu" style={styles.icon} />
      </Button>
    )}
    <Text numberOfLines={1} allowFontScaling={false} style={styles.title}>{props.title}</Text>
    {props.onPressIA ? (
      <Button onPress={props.onPressIA} style={styles.inspireButton}>
        <Icon name="arrow-right" style={styles.icon} />
      </Button>
    ) : <View style={styles.inspireButton}>
          <Text style={{width:16}}></Text>
        </View>}
  </View>

));


const styles = Style({
  container:{
    zIndex:9,
    height:50,
    position:'relative',
    alignItems:'center',
    flexDirection:'row',
    borderBottomWidth:1,
    borderColor:'#e5e5e5',
    backgroundColor:'#2d173c',
    justifyContent:'space-between'
  },
  statusBarOverlay:{
    top:-60,
    height:60,
    width:'100%',
    position:'absolute',
    backgroundColor:'#2d173c'
  },
  title:{
    width:'60%',
    fontSize:18,
    color:'white',
  },
  icon:{
    fontSize:25,
    color:'white',
  },
  menu:{
    fontSize:20,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  inspireButton:{
    fontSize:20,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
});

export default Header;
