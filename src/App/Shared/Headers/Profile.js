import React from 'react';
import { JSX, Style } from 'Modules';
import {
  Text,
  Icon,
  View,
  Image,
  Button,
  StatusBar,
} from 'Widgets';


const Header = JSX((props) => (

  <View style={styles.container}>
    <StatusBar
      backgroundColor="#2d173c"
      barStyle="light-content"
    />
    <View style={styles.statusBarOverlay} />

    <Button onPress={props.onPressNav} style={styles.backButton}>
      <Icon name="menu" style={styles.icon}/>
    </Button>
    <View style={styles.imageBox}>
      <Button onPress={props.onPressImage} style={styles.buttonPencil}>
        <Icon style={styles.iconPlus} name="camera" />
      </Button>
      <Image style={styles.profileImage} source={props.image} />
    </View>
    <Text allowFontScaling={false} style={styles.text}>{props.title}</Text>
  </View>

));


const styles = Style({
  container:{
    zIndex:9,
    position:'relative',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#2d173c',
  },
  statusBarOverlay:{
    top:-60,
    height:60,
    width:'100%',
    position:'absolute',
    backgroundColor:'#2d173c'
  },
  backButton:{
    top:-8,
    left:0,
    padding: 20,
    position:'absolute',
  },
  imageBox:{
    width:125,
    height:125,
    marginTop:40,
    borderRadius:125/2,
    position:'relative',
  },
  buttonPencil:{
    top:5,
    zIndex:1,
    width:24,
    right:10,
    height:24,
    padding:2.5,
    borderRadius:24/2,
    position:'absolute',
    alignItems:'center',
    backgroundColor:'#800',
  },
  iconPlus:{
    color:'white',
  },
  profileImage:{
    width:125,
    height:125,
    borderWidth:4,
    borderRadius:125/2,
    borderColor:'#ffffff70',
  },
  text:{
    fontSize:30,
    color:'#fff',
    marginTop:10,
    marginBottom:30,
  },
  icon:{
    fontSize:25,
    color:'white',
  },
});

export default Header;
