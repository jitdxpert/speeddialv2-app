import React from 'react';
import { JSX, Style, Screen } from 'Modules';
import {
  Text,
  View,
} from 'Widgets';
import app from 'Logic/App';


const Chart = JSX((props) => (

  <View style={styles.chart}>
    <View style={styles.chartArea}>
      <View style={styles.scale}>
        <Text style={styles.scaleText}>7 -</Text>
        <Text style={styles.scaleText}>6 -</Text>
        <Text style={styles.scaleText}>5 -</Text>
        <Text style={styles.scaleText}>4 -</Text>
        <Text style={styles.scaleText}>3 -</Text>
        <Text style={styles.scaleText}>2 -</Text>
        <Text style={styles.scaleText}>1 -</Text>
        <Text style={styles.scaleText}>0 -</Text>
      </View>
      <View style={styles.chartInner}>
        <View style={styles.barColumn}>
          <Text>{props.pending}</Text>
          <Text style={[styles.bar, styles.pending, {height:30*props.pending}]}></Text>
        </View>
        <View style={styles.barColumn}>
          <Text>{props.complete}</Text>
          <Text style={[styles.bar, styles.complete, {height:30*props.complete}]}></Text>
        </View>
      </View>
    </View>
    <View style={styles.labels}>
      <Text style={styles.label}>PENDING</Text>
      <Text style={styles.label}>COMPLETE</Text>
    </View>
  </View>

));


const styles = Style({
  chart:{
    height:280,
    borderWidth:0.5,
    borderColor:'#ddd',
    paddingVertical:20,
    paddingHorizontal:20,
    width:Screen.width-40,
    flexDirection:'column',
    backgroundColor:'white',
    borderBottomLeftRadius:5,
    borderBottomRightRadius:5,
  },
  chartArea:{
    flexDirection:'row',
  },
  scale:{
    width:16,
    marginTop:-11,
    flexDirection:'column',
  },
  scaleText:{
    height:30,
  },
  chartInner:{
    height:210,
    borderWidth:0.25,
    borderColor:'#ddd',
    flexDirection:'row',
    width:Screen.width-96,
    backgroundColor:'#f5f5f5',
  },
  labels:{
    marginTop:-10,
    marginLeft:16,
    flexDirection:'row',
  },
  label:{
    width:'50%',
    fontSize:18,
    textAlign:'center',
  },
  barColumn:{
    width:'50%',
    fontSize:18,
    alignItems:'center',
    justifyContent:'flex-end',
  },
  bar:{
    width:50,
    fontSize:16,
    elevation:1,
    color:'white',
    textAlign:'center',
  },
  pending:{
    backgroundColor:'red',
  },
  complete:{
    backgroundColor:'green',
  },
});

export default Chart;
