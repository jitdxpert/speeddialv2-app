import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  name: null,
  params: {},
  email: null,
});

page.init = (params) => {
  page.user = app.user;
  page.params = params;
};

page.onEmailProcess = () => {
  app.loading = true;
  // page.params.name = page.name;
  page.params.email = page.email;
  Api.post('/?case=EmailSpeedDial', page.params).send((res) => {
    if(res.code == 0){
      Route.pop();
    }
    app.showToast(res.text);
    app.loading = false;
  });
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
