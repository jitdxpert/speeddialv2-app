import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  videos: [],
  icon: 'videocam',
  resizeMode: 'cover',
  baseurl: app.baseurl,
  message: 'Loading...',
});

page.init = () => {
  page.user = app.user;

  page.videos = [];
  page.message = 'Loading...';
  app.loading = true;
  Api.get('/?case=GetAllVideos').send((res) => {
    page.message = null;
    if(res.code == 0){
      res.text.map((video, i) => {
        video.index = i;
        video.paused = true;
        video.loading = false;
        video.duration = '00:00';
        video.currentTime = '00:00';
        video.playPauseIcon = 'play';
      });
      page.videos = res.text;
    }else{
      page.message = res.text;
    }
    app.loading = false;
  });
};

page.onLoadStart = (video) => {
  video.loading = true;
};


page.onLoad = (data, video) => {
  video.loading = false;
  video.duration = app.formatDuration(data.duration);
};

page.onPlay = (video, index) => {
  page.videos.map((_video, i) => {
    if(index !== i){
      _video.paused = true;
      _video.playPauseIcon = 'play';
    }
  });
  video.playPauseIcon = video.paused ? 'pause' : 'play';
  video.paused = !video.paused;
};

page.onProgress = (data, video) => {
  video.loading = false;
  video.currentTime = app.formatDuration(data.currentTime);
};

page.onEnd = (video) => {
  video.paused = true;
  video.currentTime = '00:00';
  video.playPauseIcon = 'play';
};

page.onPressBack = app.onPressBack;
page.onMediaTabChange = app.onMediaTabChange;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
