import {
  DB,
  Api,
  Alert,
  Model,
  Route,
  PushNotification,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  image: app.avatar,
});

page.open = () => {

  app.user.User_FullName = app.user.User_FullName ?
  app.user.User_FullName : 'User Name';
  page.user = app.user;
  page.image = app.defaultAvatar;
  if(page.user.User_Image && page.user.User_Image !== 'img/male.jpg'){
    page.image = {uri: page.user.User_Image};
  }

};

page.close = () => {

};

page.onPressSpeedDial = () => {
  Route.set('speed-dial');
  page.onPressCloseNavbar();
};

page.onPressStatistics = () => {
  Route.set('statistics');
  page.onPressCloseNavbar();
};

page.onPressHowToUse = () => {
  Route.set('how-to-use', {back:false});
  page.onPressCloseNavbar();
};

page.onPressProfile = () => {
  Route.set('profile');
  page.onPressCloseNavbar();
};

page.onPressResources = () => {
  Route.set('resources');
  page.onPressCloseNavbar();
};

page.onPressMedia = () => {
  Route.set('media');
  page.onPressCloseNavbar();
};

page.onPressConsultation = () => {
  Route.set('consultation');
  page.onPressCloseNavbar();
};

page.onPressAbout = () => {
  Route.set('about');
  page.onPressCloseNavbar();
};

page.onPressHelp = () => {
  Route.set('help');
  page.onPressCloseNavbar();
};

page.onPressLogout = () => {
  page.onPressCloseNavbar();

  Alert.alert(
    'CONFIRM LOGOUT?',
    'Are you sure you want to logout?',
    [
      {text: 'Cancel', style: 'cancel'},
      {text: 'Logout', onPress: page.logoutProcess},
    ],
    { cancelable: true }
  );
};

page.logoutProcess = () => {
  app.loading = true;
  DB.clear();
  PushNotification.abandonPermissions();
  let params = {
    logId: page.user.Log_ID,
    userid: page.user.User_ID,
  };
  Api.post('/?case=LogoutProcess', params).send((res) => {
    if(res.code == 0){
      DB.clear();
      app.user = {};
      app.clearLocalNotifications();
      Route.set('homepage');
    }else{
      app.showToast('Unable To Logout');
    }
    app.loading = false;
  });
};

page.onPressCloseNavbar = app.onPressCloseNavbar;


export default page;
