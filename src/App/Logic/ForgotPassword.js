import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  email: null,
  bannerUri: app.bannerUri,
});

page.init = () => {
};

page.onForgotPasswordProcess = () => {
  app.loading = true;
  let params = {
    email: page.email
  };
  Api.post('/?case=ForgotProcess', params).send((res) => {
    if(res.code == 0){
      app.showToast(res.text);
      Route.set('homepage');
    }else{
      app.showToast(res.text);
    }
    app.loading = false;
  });
};

page.onPressBackToHome = () => {
  Route.set('homepage');
};

page.onPressLogin = () => {
  Route.set('signin');
};


export default page;
