import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  audios: [],
  icon: 'mic',
  resizeMode: 'cover',
  baseurl: app.baseurl,
  message: 'Loading...',
});

page.init = () => {
  page.user = app.user;

  page.audios = [];
  page.message = 'Loading...';
  app.loading = true;
  Api.get('/?case=GetAllAudios').send((res) => {
    page.message = null;
    if(res.code == 0){
      res.text.map((audio, i) => {
        audio.index = i;
        audio.paused = true;
        audio.loading = false;
        audio.duration = '00:00';
        audio.currentTime = '00:00';
        audio.playPauseIcon = 'play';
      });
      page.audios = res.text;
    }else{
      page.message = res.text;
    }
    app.loading = false;
  });
};

page.onLoadStart = (audio) => {
  audio.loading = true;
};

page.onLoad = (data, audio) => {
  audio.loading = false;
  audio.duration = app.formatDuration(data.duration);
};

page.onPlay = (audio, index) => {
  page.audios.map((_audio, i) => {
    if(index !== i){
      _audio.paused = true;
      _audio.playPauseIcon = 'play';
    }
  });
  audio.playPauseIcon = audio.paused ? 'pause' : 'play';
  audio.paused = !audio.paused;
}

page.onProgress = (data, audio) => {
  audio.loading = false;
  audio.currentTime = app.formatDuration(data.currentTime);
};

page.onEnd = (audio) => {
  audio.paused = true;
  audio.currentTime = '00:00';
  audio.playPauseIcon = 'play';
};

page.onPressBack = app.onPressBack;
page.onMediaTabChange = app.onMediaTabChange;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
