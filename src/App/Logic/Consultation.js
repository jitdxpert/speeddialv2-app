import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const calenderUri = {uri: 'https://masterpeacecoachingapp.youcanbook.me'};

const page = Model({
  user: {},
  query: null,
  state: null,
  skype: null,
  mobile: null,
  redirect: false,
  subject: 'Speed Dial FREE BIZ Consultation',
  calenderUri: calenderUri,
});

page.init = () => {
  page.user = app.user;

  Api.get('/?case=GetConsultationSubject').send((res) =>{
    if(res.code == 0 ){    
      page.subject = res.text.Page_Content;
    }
  });
};

page.onConsultationProcess = () => {
  app.loading = true;
  let params = {
    query: page.query,
    state: page.state,
    skype: page.skype,
    mobile: page.mobile,
    subject: page.subject,
    userid: page.user.User_ID
  };
  Api.post('/?case=SaveConsultation', params).send((res) => {
    if(res.code == 0){
      app.showToast(res.text);
      Route.push('appointment');
    }else{
      app.showToast(res.text);
    }
    app.loading = false;
  });
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
