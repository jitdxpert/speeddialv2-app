import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  email: null,
  password: null,
  bannerUri: app.bannerUri,
});

page.init = () => {
};

page.onLoginProcess = () => {
  app.loading = true;
  app.appStateFlag = true;
  let params = {
    email: page.email,
    password: page.password,
  };
  Api.post('/?case=SigninProcess', params).send((res) => {
    if(res.code == 0){
      app.setUserData(res);
      app.configure();
      Route.set('speed-dial');
    }else{
      app.showToast(res.text);
      app.loading = false;
    }
  });
};

page.onPressBackToHome = () => {
  Route.set('homepage');
};

page.onPressForgotPassword = () => {
  Route.push('forgot-password');
};


export default page;
