import {
  DB,
  Api,
  Model,
  Route,
  Fecha,
  Config,
  ImageCrop,
} from 'Modules';
import axios from 'axios';

import app from 'Logic/App';

const page = Model({
  user: {},
  actionSheet: null,
  image: app.avatar,
  reminderTime: null,
  reminderSwitch: true,
  showTimePicker: false,
});

page.init = () => {
  page.user = app.user;

  if(page.user.User_Image !== 'img/male.jpg'){
    page.image = {uri: page.user.User_Image};
  }

  page.reminderSwitch = page.user.User_Reminder ? true : false;
  page.reminderTime = page.user.Reminder_Time ? page.user.Reminder_Time : '22:00';
};

page.onActionSheetInit = (ref) => {
  page.actionSheet = ref;
};

page.onActionSheetPress = (index) => {
  app.appStateFlag = true;
  let option = {
    width: 200,
    height: 200,
    cropping: true
  };
  if(index === 0){
    ImageCrop.openCamera(option).then(image => {
      page.uploadToServer(image.path);
    });
  }else if(index === 1){
    ImageCrop.openPicker(option).then(image => {
      page.uploadToServer(image.path);
    });
  }
};

page.onPhotoUpload = () => {
  page.actionSheet.show();
};

page.uploadToServer = (imagePath) => {
  app.loading = true;
  page.image = {uri: imagePath};

  const form = new FormData();
  form.append('file', {
    uri: imagePath,
    type: 'image/jpeg',
    name: app.user.User_ID+'.jpg',
  });
  form.append('UserID', app.user.User_ID);

  let config = {
    onUploadProgress: function(progressEvent){
      let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
      app.showToast(percentCompleted+'%');
    },
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  };

  axios.post(Config('API_BASE')+'/?case=UploadProfileImageNew', form, config).then((res) => {
    let d = new Date();
    app.user.User_Image = res.data.image+'?v='+d.getSeconds();
    DB.set('user', app.user);
    app.showToast(res.data.text);
    app.appStateFlag = false;
    app.loading = false;
  }).catch((err) => {
    console.warn(err);
    app.loading = false;
  });
};

page.onReminderSwitch = () => {
  app.loading = true;
  page.reminderSwitch = !page.reminderSwitch;
  let reminderSwitch = 1;
  if(!page.reminderSwitch){
    reminderSwitch = 0;
  }
  app.user.User_Reminder = reminderSwitch;
  page.user.User_Reminder = reminderSwitch;
  DB.set('user', app.user);
  let params = {
    reminder: reminderSwitch,
    userid: page.user.User_ID
  };
  Api.post('/?case=UpdateReminder', params).send((res) => {
    page.showTimePicker = false;
    app.showToast(res.text);
    app.setAllLocalNotifications();
    app.loading = false;
  });
};

page.onPressReminderTime = () => {
  page.showTimePicker = true;
};

page.onReminderSelect = (value) => {
  app.loading = true;
  page.reminderTime = value;
  app.user.Reminder_Time = value;
  page.user.Reminder_Time = value;
  DB.set('user', app.user);
  page.showTimePicker = false;
  let params = {
    time: page.reminderTime,
    userid: page.user.User_ID
  };
  Api.post('/?case=UpdateReminderTime', params).send((res) => {
    app.showToast(res.text);
    app.setAllLocalNotifications();
    app.loading = false;
  });
};

page.onPressChangePassword = () => {
  Route.push('change-password');
}

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
