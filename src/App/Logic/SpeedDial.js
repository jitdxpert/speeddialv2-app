import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const d = new Date();
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const page = Model({
  user: {},
  prev: null,
  next: null,
  calendar: [],
  selected: null,
  year: d.getFullYear(),
  month: months[d.getMonth()],
  week: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
});

page.getCalendar = (params) => {
  app.loading = true;
  params.userid = page.user.User_ID;
  Api.post('/?case=SpeedDialCalendar', params).send((res) => {
    let monthYear = res.year_month_text.split(',');
    page.prev = res.prev_year_month;
    page.month = monthYear[0];
    page.year = monthYear[1];
    page.next = res.next_year_month;
    page.calendar = res.calendar;
    app.loading = false;
  });
};

page.init = () => {
  page.user = app.user;

  app.appStateFlag = false;
  let params = {};
  page.getCalendar(params);
};

page.prevMonth = () => {
  if(!page.prev){
    app.showToast('Can\'t Access Previous Month');
    return;
  }

  let params = {
    year_month: page.prev,
  };
  page.getCalendar(params);
}

page.nextMonth = () => {
  if(!page.next){
    app.showToast('Can\'t Access Next Month');
    return;
  }

  let params = {
    year_month: page.next,
  };
  page.getCalendar(params);
}

page.onPressCalendarDay = (day, key) => {
  if(!day.text){
    return;
  }

  page.selected = key;
  app.date = day;
};

page.onPressJournal = () => {
  if(!app.date.date){
    app.showToast('Choose a Date');
    return;
  }
  Route.push('journal');
};

page.onPressHowToUse = () => {
  Route.push('how-to-use', {back:true});
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
