import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  bannerUri: app.bannerUri,
});

page.init = () => {
};

page.onPressSignin = () => {
  Route.push('login');
};

page.onPressSignup = () => {
  Route.push('register');
};


export default page;
