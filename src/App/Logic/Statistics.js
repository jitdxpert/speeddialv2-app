import {
  DB,
  Api,
  Model,
  Fecha,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  total: 0,
  pending: 0,
  percent: 0,
  complete: 0,
  choosenDate: null,
  showDatePicker: false,
});

page.init = (user) => {
  page.user = app.user;

  let d = new Date();
  page.choosenDate = d.getUTCFullYear()+'-'+('0'+(d.getUTCMonth()+1)).slice(-2)+'-'+('0'+d.getUTCDate()).slice(-2);
  page.getStatistics();
};

page.getStatistics = () => {
  app.loading = true;
  let params = {
    date: page.choosenDate,
    userid: page.user.User_ID,
  };
  Api.post('/?case=GetStats', params).send((res) => {
    page.total = res.TotalRocks ? res.TotalRocks : 0;
    page.pending = res.PendingRocks ? res.PendingRocks : 0;
    page.complete = res.CompleteRocks ? res.CompleteRocks : 0;
    if(!page.total){
      page.percent = parseInt((page.complete * 100));
    }else{
      page.percent = parseInt((page.complete * 100) / page.total);
    }
    app.loading = false;
  });
}

page.onPressChangeDate = () => {
  page.showDatePicker = true;
};

page.onSelectDate = (value) => {
  app.loading = true;
  page.choosenDate = value;
  page.showDatePicker = false;
  page.getStatistics();
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
