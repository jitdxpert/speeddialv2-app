import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const calenderUri = {uri: 'https://masterpeacecoachingapp.youcanbook.me'};

const page = Model({
  user: {},
  calenderUri: calenderUri,
});

page.init = () => {
  page.user = app.user;
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
