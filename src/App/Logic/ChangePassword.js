import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  curPass: null,
  newPass: null,
  conPass: null,
});

page.init = () => {
  page.user = app.user;
};

page.onChangePassProcess = () => {
  app.loading = true;
  let params = {
    userid: page.user.User_ID,
    current: page.curPass,
    new: page.newPass,
    confirm: page.conPass,
  };
  Api.post('/?case=ChangePassword', params).send((res) => {
    if(res.code == 0){
      Route.pop();
    }
    app.showToast(res.text);
    app.loading = false;
  });
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
