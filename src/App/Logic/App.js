import {
  DB,
  Api,
  Fecha,
  Model,
  Route,
  Alert,
  Config,
  Keyboard,
  PushNotification,
  PushNotificationIOS,
} from 'Modules';
import { Toast } from 'Widgets';

const defaultAvatar = require('Assets/imgs/user.png');
const bannerImage = require('Assets/imgs/banner.jpg');

const date = new Date();
const tzOffset = -date.getTimezoneOffset()/60;

const app = Model({
  user: {},
  date: {},
  device: {},
  activeTab: 0,
  error: false,
  online: false,
  loading: false,
  tzoffset: tzOffset,
  avatar: defaultAvatar,
  bannerUri: bannerImage,
  baseurl: Config('WEB_BASE'),
});

app.pushRedirect = false;
app.appStateFlag = false;
app.initPageTimer = null;

app.onMediaTabChange = (index) => {
  if(index === 0){
    Route.set('media');
  }else if(index === 1){
    Route.set('audio');
  }
  app.activeTab = index;
};

app.onPressOpenNavbar = () => {
  Keyboard.dismiss();
  Route.openDrawer();
};

app.onPressCloseNavbar = () => {
  Route.closeDrawer();
};

app.onPressBack = () => {
  Route.pop();
};

app.showToast = (text) => {
  Toast.show(text);
};

app.formatDuration = (second) => {
  date.setHours(0);
  date.setMinutes(0);
  date.setSeconds(second);

  return Fecha.format(date, 'mm:ss');
}

app.onLocalNotification = (notification) => {
  let data = {
    back: false,
    date: notification.data.date,
    userid: notification.data.userid,
  };
  Alert.alert(
    'SpeedDial The Universe',
    notification.message,
    [
      {text: 'Show', onPress: () => Route.set('inspired-actions', data)},
      {text: 'Cancel', style: 'cancel'}
    ],
    { cancelable: true }
  );

};

app.onPushNotification = (notification) => {

  if(app.pushRedirect){
    return;
  }

  var data = notification;
  if(notification.alert){
    data = notification.alert;
  }
  if(notification.foreground){
    Alert.alert(
      'SpeedDial The Universe',
      data.title+"\n\n"+data.body,
      [
        {text: 'Show', onPress: () => app.notificationLinkRedirection(data)},
        {text: 'Cancel', style: 'cancel'}
      ],
      { cancelable: true }
    );
  }else{
    if(data.link){
      app.notificationLinkRedirection(data);
    }else{
      Route.set('resources');
    }
  }

};



app.setUserData = (res) => {
  res.text.Log_ID = res.LogID;
  app.user = res.text;
  DB.set('user', res.text);
};

app.configure = () => {
  app.createLogId();
  app.setAllLocalNotifications();

  let params = {
    device: app.device.os,
    regId: app.device.token,
    userid: app.user.User_ID,
  };
  Api.post('/?case=SaveRegistrationId', params).send((res) => {
    console.warn('SaveRegistrationId', res);
  });
};

app.notificationLinkRedirection = (notification) => {
  app.pushRedirect = true;
  setTimeout(() => {
    app.pushRedirect = false;
  }, 60000);

  var params = {
    link: notification.link,
    title: notification.title
  };
  Route.set('notification', params);
};

app.createLogId = () => {
  let params = {
    tzoffset: app.tzoffset,
    userid: app.user.User_ID,
  };
  Api.post('/?case=CreateLogID', params).send((res) => {
    app.user.Log_ID = res.LogID;
    app.user.User_TzOffset = app.tzoffset;
    DB.set('user', app.user);
    console.warn('CreateLogID', res);
  });
};

app.localNotification = (date, today) => {
  let localId = date.split('-').join('');
  let params = {
    userID: app.user.User_ID,
    localID: localId,
    notiDate: date
  };
  app.saveLocalNotification(params);

  if(app.user.User_Reminder == 0){
    app.clearLocalNotifications();
    return;
  }

  let d = new Date();
  let nowAt = new Date(today);
  nowAt.setHours(d.getHours());
  nowAt.setMinutes(d.getMinutes());

  let notiTime = app.user.Reminder_Time.split(':');
  let notifyAt = new Date(date);
  notifyAt.setHours(parseInt(notiTime[0]));
  notifyAt.setMinutes(parseInt(notiTime[1]));

  PushNotification.cancelLocalNotifications({id: ''+localId});
  if(nowAt >= notifyAt){
    return;
  }

  PushNotification.localNotificationSchedule({
    id: ''+localId,
    date: notifyAt,
    title: 'SpeedDial The Universe',
    message: 'How many Inspired Actions have you accomplished on '+Fecha.format(notifyAt, 'dddd MMMM Do, YYYY')+'?',
    userInfo: {
      local: 1,
      date: date,
      path: 'inspired-actions',
      userid: app.user.User_ID
    },
    playSound: true,
    soundName: 'default',
  });
};

app.saveLocalNotification = (params) => {
  Api.post('/?case=SaveLocalNotify', params).send((res) => {
    console.warn('SaveLocalNotify', res);
  });
};

app.setAllLocalNotifications = () => {
  let params = {
    userID: app.user.User_ID
  };
  Api.post('/?case=GetLocalNotificationByUser', params).send((res) => {
    if(res.code == 0){
      for(let i in res.notifications){
        app.localNotification(res.notifications[i].Noti_Date, res.today);
      }
    }
    console.warn('GetLocalNotificationByUser', res);
  });
};

app.clearLocalNotifications = () => {
  PushNotification.cancelAllLocalNotifications();
};

app.generateHtml = (res) => {
  let doc = '';

  if(res.text.journal.Answer_1){
    doc += '<label style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span>Today, I appreciate:</span>';
    doc += '</label>';
    doc += '<label style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_1+'</p>';
    doc += '</label>';
  }
  if(res.text.journal.Answer_2){
    doc += '<label style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span>Today my dominant feeling will be:</span>';
    doc += '</label>';
    doc += '<label style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_2+'</p>';
    doc += '</label>';
  }

  doc += '<label style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
    doc += '<span>My 6 Inspired Actions for today are</span>';
  doc += '</label>';

  doc += '<div style="margin:0 0 10px 0;">';
    let i = 1;
    for(let r in res.text.rockAns){
      if(res.text.rockAns[r].Answer){
        doc += '<label style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0;">';
          doc += '<p>'+i+'. '+res.text.rockAns[r].Answer+'</p>';
        doc += '</label>';
      }
      i++;
    }
  doc += '</div>';

  if(res.text.journal.Answer_3){
    doc += '<label style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span>Everything else I desire is being delegated to the Universe. Thank you Universe, for handling the following for me!</span>';
    doc += '</label>';
    doc += '<label style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_3+'</p>';
    doc += '</label>';
  }
  if(res.text.journal.Answer_4){
    doc += '<label style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span>My Wild Hair Intention. Wouldn\'t it be awesome today if:</span>';
    doc += '</label>';
    doc += '<label style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_4+'</p>';
    doc += '</label>';
  }
  if(res.text.journal.Answer_5){
    doc += '<label style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span>One of my desires has manifested in the most perfect way. It unfolded like this...</span>';
    doc += '</label>';
    doc += '<label style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_5+'</p>';
    doc += '</label>';
  }
  if(res.text.journal.Answer_6){
    doc += '<label style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span>The most important thing for me to focus on today is...</span>';
    doc += '</label>';
    doc += '<label style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_6+'</p>';
    doc += '</label>';
  }

  return doc;
};


export default app;
