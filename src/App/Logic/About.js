import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  content: 'Loading...',
});

page.init = () => {
  page.user = app.user;
  app.loading = true;
  Api.get('/?case=GetAboutUsContent').send((res) => {
    app.loading = false;
    page.content = unescape(res.text.Page_Content);
  });
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
