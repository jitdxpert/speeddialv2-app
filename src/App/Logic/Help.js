import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  concern: null,
});

page.init = () => {
  page.user = app.user;
};

page.onHelpProcess = () => {
  app.loading = true;
  let params = {
    concern: page.concern,
    userid: page.user.User_ID
  };
  Api.post('/?case=SaveSupport', params).send((res) => {
    if(res.code == 0){
      app.showToast(res.text);
      page.concern = null;
    }else{
      app.showToast("Field can't be blank");
    }
    app.loading = false;
  });
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
