import {
  DB,
  Api,
  Model,
  Route,
  Config,
} from 'Modules';
import {GoogleSignin, statusCodes} from 'react-native-google-signin';
import {AccessToken, GraphRequest, LoginManager, GraphRequestManager} from 'react-native-fbsdk';

import app from 'Logic/App';

const page = Model({
  bannerUri: app.bannerUri,
});

page.init = () => {
  GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true }).then(() => {
    console.warn('Play Service Available');
  }).catch((err) => {
    console.warn('Play Service Error', err.message);
  });

  GoogleSignin.configure({
    scopes: ['https://www.googleapis.com/auth/drive.readonly'],
    offlineAccess: false,
    webClientId: Config('WEB_CLIENT_ID'),
    iosClientId: Config('IOS_CLIENT_ID'),
  });
};


page.onPressFbLogin = () => {
  app.appStateFlag = true;
  LoginManager.logInWithReadPermissions([
    'public_profile', 'email'
  ]).then((result) => {
    if(result.isCancelled){
      console.warn('Login Cancel');
    }else{
      app.loading = true;
      AccessToken.getCurrentAccessToken().then((data) => {
        const infoRequest = new GraphRequest('/me?fields=id,name,email,picture.height(150)', null,  page.facebookInfoCallback);
        new GraphRequestManager().addRequest(infoRequest).start();
      });
    }
  }, (error) => {
    console.warn('Login Fail', error);
  });
};

page.facebookInfoCallback = (error, result) => {
  if(error){
    return;
  }

  let pictureUrl = null;
  if(result.picture && result.picture.data && result.picture.data.url){
    pictureUrl = result.picture.data.url;
  }
  var params = {
    sex: null,
    screen: null,
    sId: result.id,
    media: 'facebook',
    name: result.name,
    image: pictureUrl,
    email: result.email,
  };
  Api.post('/?case=SocialSigninProcess', params).send((res) => {
    if(res.code == 0){
      app.setUserData(res);
      app.configure();
      Route.set('speed-dial');
    }else{
      app.showToast(res.text);
      app.loading = false;
    }
  });
};


page.onPressGpLogin = async () => {
  app.appStateFlag = true;
  try{
    app.loading = true;
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    let params = {
      sex: null,
      screen: null,
      media: 'google',
      sId: userInfo.user.id,
      name: userInfo.user.name,
      email: userInfo.user.email,
      image: userInfo.user.photo+'?sz=150',
    };
    Api.post('/?case=SocialSigninProcess', params).send((res) => {
      if(res.code == 0){
        res.text.Log_ID = res.LogID;
        app.user = res.text;
        DB.set('user', res.text);
        app.configure();
        Route.set('speed-dial');
      }else{
        app.showToast(res.text);
        app.loading = false;
      }
    });
  }catch(error){
    if(error.code === statusCodes.SIGN_IN_CANCELLED){
      console.warn('Login was cancelled');
    }else if(error.code === statusCodes.IN_PROGRESS){
      console.warn('Login in progress');
    }else if(error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE){
      console.warn('Play services not available');
    }else{
      console.warn('Error happen while login', error);
    }
    app.loading = false;
  }
};

page.onPressLogin = () => {
  Route.push('signin');
};

page.onPressBackToHome = () => {
  Route.pop();
};


export default page;
