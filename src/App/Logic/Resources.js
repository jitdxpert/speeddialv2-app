import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  events: [],
  message: null,
  buyBtn: 'BUY NOW!',
  freeBtn: 'DOWNLOAD!',
  buttonColor: '#e0592b',
});




page.init = () => {
  page.user = app.user;

  app.loading = true;
  Api.get('/?case=GetButtonContent').send((res) => {
    if(res.code == 0){
      page.buyBtn = res.text.buyBtn.Page_Content;
      page.freeBtn = res.text.freeBtn.Page_Content;
      page.buttonColor = res.text.buttonColor.Page_Content;
    }
  });

  Api.get('/?case=GetAllEvents').send((res) => {
    if(res.code == 0){
      page.events = res.text;
    }else{
      page.message = res.text;
    }
    app.loading = false;
  });
};

page.onPressEventLink = (event) => {
  Route.push('event-redirection', event);
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
