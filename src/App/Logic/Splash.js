import {
  DB,
  Api,
  Model,
  Route,
  Alert,
  NetInfo,
  AppState,
  BackHandler,
  SplashScreen,
  PushNotification,
} from 'Modules';

import app from 'Logic/App';

const splashImage = require('Assets/imgs/splash.gif');

const page = Model({
  splashUri: splashImage,
});

let hasUser = false;
page.init = () => {

  setTimeout(() => {
    SplashScreen.hide();
  }, 1000);

  DB.get('user').then((user) => {
    user = (user != null ? user : {});
    if(typeof user.User_ID !== 'undefined'){
      hasUser = true;
      app.user = user;
    }
  });

  app.initPageTimer = setTimeout(() => {
    if(!hasUser){
      Route.set('homepage');
    }else{
      app.configure();
      Route.set('speed-dial');
    }
  }, 4000);
};

AppState.addEventListener('change', () => {
  setTimeout(() => {
    SplashScreen.hide();
  }, 1000);

  NetInfo.isConnected.addEventListener('connectionChange', (isConnected) => {
    app.online = isConnected;
    if(!app.online){
      Alert.alert(
        'NO INTERNET',
        'Sorry, no internet connection detected. Please reconnect and try again.',
        [
          {text: 'OK', onPress: () => BackHandler.exitApp()},
        ],
        { cancelable: false }
      );
    }
  });

  if(AppState.currentState !== 'active'){
    let params = {
      logId: app.user.Log_ID,
      userid: app.user.User_ID,
    };
    Api.post('/?case=LogoutProcess', params).send((res) => {
      app.user.Log_ID = 0;
      DB.set('user', app.user);
    });
  }else{
    PushNotification.popInitialNotification((notification) => {
      if(notification){
        clearTimeout(app.initPageTimer);
        app.onNotification(notification);
      }
    });
  }
});


export default page;
