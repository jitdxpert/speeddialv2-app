import {
  DB,
  Api,
  Model,
  Route,
  Linking,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  hasBack: false,
  content: 'Loading...',
});

page.init = (back) => {
  page.user = app.user;
  page.hasBack = back.back;

  app.loading = true;
  Api.get('/?case=GetSpeedDialContent').send((res) => {
    app.loading = false;
    page.content = unescape(res.text.Page_Content);
  });
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
