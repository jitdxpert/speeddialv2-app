import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import RNPrint from 'react-native-print';

import app from 'Logic/App';

const page = Model({
  user: {},
  date: {},
  journalId: 0,
  question1: null,
  question2: null,
  question3: null,
  question4: null,
  question5: null,
  question6: null,
  rocks: {
    rock1: null,
    rockId1: 0,
    rock2: null,
    rockId2: 0,
    rock3: null,
    rockId3: 0,
    rock4: null,
    rockId4: 0,
    rock5: null,
    rockId5: 0,
    rock6: null,
    rockId6: 0,
  },
  emailPrint: false,
  actionSheet: null,
});

page.init = () => {
  page.exit();

  page.user = app.user;
  page.date = app.date;

  app.loading = true;
  let params = {
    date: page.date.date,
    userid: page.user.User_ID,
  };
  Api.post('/?case=GetSpeedDial', params).send((res) => {
    if(res.code == 0){
      page.emailPrint = true;

      let rocks = {};
      for(var r in res.text.rockIds){
        rocks[r] = res.text.rockIds[r].Rock_ID;
      }
      for(var r in res.text.rockAns){
        rocks[r] = res.text.rockAns[r].Answer;
      }
      page.rocks = rocks;

      page.question1 = res.text.journal.Answer_1;
      page.question2 = res.text.journal.Answer_2;
      page.question3 = res.text.journal.Answer_3;
      page.question4 = res.text.journal.Answer_4;
      page.question5 = res.text.journal.Answer_5;
      page.question6 = res.text.journal.Answer_6;
      page.journalId = res.text.journal.Journal_ID;
    }
    app.loading = false;
  });
};

page.exit = () => {
  page.journalId = 0;
  page.question1 = null;
  page.question2 = null;
  page.question3 = null;
  page.question4 = null;
  page.question5 = null;
  page.question6 = null;
  page.rocks.rock1 = null;
  page.rocks.rockId1 = 0;
  page.rocks.rock2 = null;
  page.rocks.rockId2 = 0;
  page.rocks.rock3 = null;
  page.rocks.rockId3 = 0;
  page.rocks.rock4 = null;
  page.rocks.rockId4 = 0;
  page.rocks.rock5 = null;
  page.rocks.rockId5 = 0;
  page.rocks.rock6 = null;
  page.rocks.rockId6 = 0;
  page.emailPrint = false;
};

page.onPressSaveJournal = () => {
  app.loading = true;

  if(!page.rocks.rock1 && !page.rocks.rock2 && !page.rocks.rock3 && !page.rocks.rock4 && !page.rocks.rock5 && !page.rocks.rock6){
    app.loading = false;
    app.showToast("Please fillup atleast one Inspired Action");
  }else{
    let params = {
      date: page.date.date,
      userid: page.user.User_ID,
      journalId: page.journalId,
      question1: page.question1,
      question2: page.question2,
      question3: page.question3,
      question4: page.question4,
      question5: page.question5,
      question6: page.question6,
      rocks: {
        rock1: page.rocks.rock1,
        rockId1: page.rocks.rockId1,
        rock2: page.rocks.rock2,
        rockId2: page.rocks.rockId2,
        rock3: page.rocks.rock3,
        rockId3: page.rocks.rockId3,
        rock4: page.rocks.rock4,
        rockId4: page.rocks.rockId4,
        rock5: page.rocks.rock5,
        rockId5: page.rocks.rockId5,
        rock6: page.rocks.rock6,
        rockId6: page.rocks.rockId6,
      }
    };
    Api.post('/?case=SaveSpeedDial', params).send((res) => {
      app.loading = false;
      app.showToast(res.text);
      app.date.hasItem = res.hasItem;
      page.date.hasItem = res.hasItem;
      app.localNotification(app.date.date, res.today);
      page.emailPrint = true;
    });
  }
};

page.onActionSheetInit = (ref) => {
  page.actionSheet = ref;
};

page.onActionSheetPress = (index) => {
  if(!page.emailPrint){
    app.showToast('Please save the journal first');
    return;
  }

  if(index === 0){
    app.loading = true;
    let params = {
      date: page.date.date,
      userid: page.user.User_ID,
    };
    Api.post('/?case=GetSpeedDial', params).send((res) => {
      if(res.code == 0){
        let htmlString = app.generateHtml(res);
        RNPrint.print({html: htmlString});
      }else{
        app.showToast('Unable To Print');
      }
      app.loading = false;
    });
  }else if(index === 1){
    let params = {
      question1: page.question1,
      question2: page.question2,
      question3: page.question3,
      question4: page.question4,
      question5: page.question5,
      question6: page.question6,
      rocks: {
        rock1: page.rocks.rock1,
        rock2: page.rocks.rock2,
        rock3: page.rocks.rock3,
        rock4: page.rocks.rock4,
        rock5: page.rocks.rock5,
        rock6: page.rocks.rock6,
      },
    };
    Route.push('email', params);
  }
};

page.onPressEmailPrint = () => {
  page.actionSheet.show();
};

page.onPressInspireAction = () => {
  if(page.date.hasItem > 0){
    let data = {
      back: true,
      date: app.date.date,
      userid: app.user.User_ID,
    };
    Route.push('inspired-actions', data);
  }else{
    app.showToast('Please save the journal first');
  }
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
