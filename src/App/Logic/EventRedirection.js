import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  link: {},
  title: '',
});

page.init = (event) => {
  page.user = app.user;

  if(event.Event_Link){
    page.title = event.Event_Name;
    page.link = {uri: event.Event_Link};
  }else{
    page.title = event.Product_Name;
    page.link = {uri: event.Product_Link};
  }
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
