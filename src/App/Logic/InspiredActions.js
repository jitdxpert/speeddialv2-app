import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  rocks: {},
  items: [],
  date: null,
  hasBack: false,
});

page.init = (data) => {
  page.date = data.date;
  page.hasBack = data.back;
  page.userid = data.userid;

  console.warn(data);

  app.loading = true;
  let params = {
    date: page.date,
    userid: page.userid,
  };
  Api.post('/?case=GetSpeedDial', params).send((res) => {
    let rocks = [];
    let check = {};
    if(res.code == 0){
      for(let r in res.text.rockIds){
        rocks.push(res.text.rockIds[r]);
        if(res.text.rockIds[r].Rock_Status == 'complete'){
          check[res.text.rockIds[r].Rock_ID] = true;
        }else{
          check[res.text.rockIds[r].Rock_ID] = false;
        }
      }
    }
    page.items = rocks;
    page.rocks.check = check;
    app.loading = false;
  })
};

page.onToggleComplete = (item) => {
  if(item.Rock_Status == 'pending'){
    item.Rock_Status = 'complete';
  }else{
    item.Rock_Status = 'pending';
  }
};

page.saveInspiredActions = () => {
  app.loading = true;
  let check = {};
  page.items.map((item, i) => {
    if(item.Rock_Status == 'complete'){
      check[item.Rock_ID] = true;
    }else{
      check[item.Rock_ID] = false;
    }
  });
  let params = {
    check: check,
    userid: page.userid,
    date: page.date,
  };
  Api.post('/?case=SaveStatus', params).send((res) => {
    app.showToast(res.text);
    Route.set('speed-dial');
    app.loading = false;
  });
};

page.onPressBack = app.onPressBack;
page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
