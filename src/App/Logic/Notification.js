import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  user: {},
  link: {},
  title: '',
});

page.init = (notification) => {
  page.user = app.user;

  page.title = notification.title;
  page.link = {uri: notification.link};
};

page.onPressBack = () => {
  Route.set('speed-dial');
};

page.onPressOpenNavbar = app.onPressOpenNavbar;


export default page;
