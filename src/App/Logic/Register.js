import {
  DB,
  Api,
  Model,
  Route,
} from 'Modules';

import app from 'Logic/App';

const page = Model({
  name: null,
  email: null,
  password: null,
  bannerUri: app.bannerUri,
});

page.init = () => {
};

page.onRegisterProcess = () => {
  app.loading = true;
  app.appStateFlag = true;
  let params = {
    name: page.name,
    email: page.email,
    password: page.password,
  };
  Api.post('/?case=SignupProcess', params).send((res) => {
    if(res.code == 0){
      app.setUserData(res);
      app.configure();
      Route.set('how-to-use', {back:false});
    }else{
      app.showToast(res.text);
      app.loading = false;
    }
  });
};

page.onPressLogin = () => {
  Route.push('login');
};

page.onPressBackToHome =() => {
  Route.set('homepage');
};


export default page;
